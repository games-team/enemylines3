#ifndef __el3__skybox_h
#define __el3__skybox_h

#include <iostream>

namespace el3 {


class Skybox {

public:

	static void draw();
	static void inner_draw(int seed=0);
	static void gen_dl(int seed=0);

};


} //namespace

#endif
