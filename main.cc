#include "SDL.h"
#include "SDL_opengl.h"

#include <iostream>

#include "config.h"
#include "game.h"
#include "util.h"
#include "random.h"
#include "menu.h"
#include "help.h"
#include "font_ogl.h"
#include "font_data.h"
#include "font_ttf.h"
#include "font.h"
#include "config.h"
#include "audio.h"
#include "skybox.h"
#include "tips.h"

#include "models/all.h"

#include "tweak/tweak.h"

#include "release.h"

namespace PRJID {

void light() {
    GLfloat light0_pos[4]   = { 0.0, 5.0, .0, 0.0 };
    GLfloat light0_color[4] = { .6, .6, .6, 1.0 }; 
    GLfloat light1_pos[4]   = {  20.0, 5.0, 20.0, 0.0 };
    GLfloat light1_color[4] = { .4, .4, 1, 1.0 };  

    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

    glLightfv(GL_LIGHT0, GL_POSITION, light0_pos);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light0_color);
    glLightfv(GL_LIGHT1, GL_POSITION, light1_pos);
    glLightfv(GL_LIGHT1, GL_DIFFUSE,  light1_color);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHTING);

}

void load_audio(std::string dir) {
	std::string path;
	if (dir=="") {
		path="data/";
		std::cout << "  Looking in " << path << std::endl;
		if (Audio::load(path)) return;
		
		path="/usr/local/share/";
		path+=PATHNAME;
		path+="/";
		std::cout << "  Looking in " << path << std::endl;
		if (Audio::load(path)) return;

		path="/usr/share/";
		path+=PATHNAME;
		path+="/";
		std::cout << "  Looking in " << path << std::endl;
		if (Audio::load(path)) return;

	}

	if (Audio::load(dir)) return;
	std::cerr << "  Audio files not found ... They are optional however so feel free to ignore this." << std::endl;
}
void load_font(std::string dir) {
	std::string path;
	if (dir=="") {
		path="data/";
		std::cout << "  Looking in " << path << std::endl;
		if (Font::load(path)) return;
		
		path="/usr/local/share/";
		path+=PATHNAME;
		path+="/";
		std::cout << "  Looking in " << path << std::endl;
		if (Font::load(path)) return;

		path="/usr/share/";
		path+=PATHNAME;
		path+="/";
		std::cout << "  Looking in " << path << std::endl;
		if (Font::load(path)) return;

	}

	if (Font::load(dir)) return;
	std::cerr << "  Font file not found ... Using fallback." << std::endl;

	Font::load_fallback();
}
void load_resources(std::string dir) {
	std::cout << " Fonts: " << std::endl;
	load_font(dir);
	std::cout << " Audio: " << std::endl;
	load_audio(dir);
}


SDL_Surface *screen;

bool setmode(int w,int h,bool fullscreen) {
	if(!(screen=SDL_SetVideoMode(w,h,0,SDL_OPENGL|SDL_RESIZABLE | ((fullscreen) ? SDL_FULLSCREEN : 0)))) {
		std::cerr <<"SDL_SetVideoMode error " << w << "x" << h << " " << fullscreen << "  " << SDL_GetError() << std::endl;
		return false;
	}
	Config::resolution(w,h);
	SDL_WM_SetCaption(FULLNAME,"");
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(60.0f,(GLfloat)screen->w/(GLfloat)screen->h,0.15f,75.0f);
   glViewport(0, 0, screen->w, screen->h);
   glMatrixMode(GL_MODELVIEW);

   glEnable(GL_DEPTH_TEST);
	return true;
}

typedef enum e_menuids {
	M_POP=-1,
	M_NONE=0,
	M_START_EASY=1,
	M_START_MEDIUM=2,
	M_START_HARD=3,
	M_START_NIGHTMARE=4,
	M_QUIT=5
};


int main(int w,int h,bool fullscreen,bool audio,std::string dir) {
   std::cout<<std::endl<<std::endl<<FULLNAME << " "<< VERSION << std::endl;
   std::cout<<"\t Copyright (C) 2005-2006" << std::endl;
   std::cout<<"\t " << EL_URL << std::endl;
   std::cout<<"\t Using libSDL,SDL_mixer under the terms of the GNU LGPL" << std::endl;
	std::cout << std::endl;
   /*std::cout<<"This program is free software; you can redistribute it and/or"<<std::endl;
   std::cout<<"modify it under the terms of the GNU General Public License," <<std::endl;
   std::cout<<"version 2, as published by the Free Software Foundation; You"<<std::endl;
   std::cout<<"should have received a copy of the GNU General Public License"<<std::endl;
   std::cout<<"along with this program; if not, write to the Free Software" <<std::endl;
   std::cout<<"Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA."<<std::endl;
   std::cout << std::endl << std::endl;*/

#ifdef NOAUDIO
	std::cout << "compiled without audio support." << std::endl;
#endif
#ifdef NOTTF
	std::cout << "compiled without ttf support." << std::endl;
#endif
#ifdef NOCURL
	std::cout << "compiled without online hiscore support." << std::endl;
#endif


	//Tweak::tick();

	std::cout << "Starting video subsys. " << std::endl;
	if(SDL_Init(SDL_INIT_VIDEO)==-1) {
		std::cerr <<"SDL_Init error SDL_INIT_VIDEO" << std::endl;
		return EXIT_FAILURE;
	}
	atexit(SDL_Quit);

	std::cout << "Setting video mode. " << std::endl;
	if (!setmode(w,h,fullscreen)) {
		std::cerr <<"SDL_SetVideoMode error" << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Starting audio subsys. " << std::endl;
	if (audio) {
		if (!Audio::init()) {
			std::cerr << "  Error in Mix_OpenAudio" << std::endl;
		}
	} else { Audio::off(); }


	std::cout << "Loading resources: " << std::endl;

	load_resources(dir);

	std::cout << "Testing speed: " << std::endl;
	if (!testspeed()) { return EXIT_SUCCESS; }


	std::cout << "Uploading Models: " << std::endl;
	models::robot::gen_dl();	
	//models::robot2::gen_dl();	
	models::key::gen_dl();	
	models::skip::gen_dl();	
	models::weapon::gen_dl();	
	models::pill::gen_dl();	
	Skybox::gen_dl();
	error();

	Audio::play(AS_TITLE,AC_TITLE);
	light();

	Random::sseed();

	Game *game=NULL;
	Menu menu(C3(245,140));


	menu.add(Menuitem("Start Easy",M_START_EASY));
	menu.add(Menuitem("Start Medium",M_START_MEDIUM));
	menu.add(Menuitem("Start Hard",M_START_HARD));
	menu.add(Menuitem("Start Nightmare",M_START_NIGHTMARE));
	menu.add(Menuitem("Quit",M_QUIT));

   SDL_Event event;
	int timestamp,oldtimestamp=0;

	bool handled;

	unsigned int unhandled=0;
	unsigned int showunhandled=0;

	unsigned int delay;
	unsigned int record=0;


	Tips::roll();
   while (true) {

     	while  (SDL_PollEvent(&event)) {
			handled=false;
			if (game) { 
				handled=game->handle_event(event); 
			} else {
				e_menuids id=(e_menuids)menu.handle_event(event); 
				unhandled=6;
				switch (id) {
					case M_START_EASY: 
						game=new Game(NULL,D_EASY); 
						menu.reset();
						break;
					case M_START_MEDIUM: 
						game=new Game(NULL,D_MEDIUM); 
						menu.reset();
						break;
					case M_START_HARD: 
						game=new Game(NULL,D_HARD); 
						menu.reset();
						break;
					case M_START_NIGHTMARE: 
						game=new Game(NULL,D_NIGHTMARE); 
						menu.reset();
						break;
					case M_QUIT:
						SDL_Quit();
						return EXIT_SUCCESS;
						break;
					default: break;
				}
			}
			if (!handled) switch (event.type) {
					case SDL_VIDEORESIZE:
						setmode(event.resize.w,event.resize.h,false);
						break;
      			case SDL_QUIT: return EXIT_SUCCESS; break;

					case SDL_KEYDOWN: switch (event.key.keysym.sym) {
						case SDLK_w:
						case SDLK_a:
						case SDLK_s:
						case SDLK_d:
						case SDLK_LEFT:
						case SDLK_RIGHT:
						case SDLK_UP:
						case SDLK_DOWN:
						case SDLK_RETURN:
						case SDLK_ESCAPE:
						case SDLK_p:
						case SDLK_SPACE: break;

						case SDLK_F1:
						case SDLK_h:
							if (showunhandled>0) {showunhandled=0; break; }
							unhandled=6; break;

						case SDLK_r: Config::toggle_mouse_reverse(); break;
						case SDLK_g: Config::toggle_show_gun(); break;
						case SDLK_m: togglemouselock(); break;
						case SDLK_KP_ENTER:
						case SDLK_f: SDL_WM_ToggleFullScreen(screen); break;
						case SDLK_F10: screenshot(screen->w,screen->h); break;
						case SDLK_F11: 
							Config::toggle_record();
							record=0;
							break;
						case SDLK_v: Audio::toggle(); break;
						case SDLK_t: Tips::cycle(); break;
						default: 
							//unhandled key
							unhandled++;
							break;
					}
			}
		}
		//if (Random::sget(50)==10) Tweak::tick();


		glLoadIdentity();
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		if (game) {
			game->handle_state();


			int diff;
			timestamp=SDL_GetTicks();
			diff=timestamp-oldtimestamp;
			oldtimestamp=timestamp;



			game->tick(diff);


			game->draw();
			E_GameState state=game->get_state();
			if (state!=GS_PLAYING&&state!=GS_FINISHED) {
				if (state==GS_NEXTLEVEL) {
					Game *newgame=new Game(game);
					delete game;
					game = newgame;
				} else {
					delete game;
					game=NULL;
					Tips::roll();
				}
			}
			if (unhandled>5) {
				showunhandled=1000;
				unhandled=0;
			}

			if (showunhandled>0) {
				showunhandled--;
				ortho2d();
				glDisable(GL_LIGHTING);
				Help::controls();
				Help::gameplay();
				glEnable(GL_LIGHTING);
				ortho2d_off();
			}
			delay=0;
		} else {
			if (menu.isactive()==false) { SDL_Quit(); return EXIT_SUCCESS; }
			ortho2d();
			glDisable(GL_LIGHTING);
			menu_bg();
			glEnable(GL_LIGHTING);
			ortho2d_off();

			glPushMatrix();
			glTranslatef(-2.0f,2.0f,-5.0f);
			models::robot::dldraw();
			glPopMatrix();
			glPushMatrix();
			glTranslatef(2.0f,2.0f,-5.0f);
			models::robot::dldraw();
			glPopMatrix();

			ortho2d();
			glDisable(GL_LIGHTING);

			Tips::draw();


			Help::title();
			menu.draw();


			Help::gameplay();
			Help::controls();
			glEnable(GL_LIGHTING);
			ortho2d_off();
			delay=20;
		}


		SDL_GL_SwapBuffers();
		if (Config::record()) {
			record++;
			screenshot(screen->w,screen->h,record);
		}

		SDL_Delay(delay);
		Audio::tick();
		error();

	}
}

} // namespace

int main(int argc,char *argv[]) {
	//unsigned int w=640; unsigned int h=480;
	unsigned int w=800; unsigned int h=600;
	bool fullscreen=false;
	bool offline=false;
	bool audio=true;
	std::string dir="";
	std::string name="";
	int league=0;

	std::istringstream istr;
	if (argc>1) {
		for (int i=1;i<argc;i++) {
			std::string arg(argv[i]);
			if (arg=="-offline") {
				offline=true;
				continue;
			} 
			if (arg=="-fullscreen") {
				fullscreen=true;
				continue;
			} 
			if (arg=="-noaudio") {
				audio=false;
				continue;
			} 
			if (arg=="-name") {
				if (i==argc-1) {
					std::cerr << "Argument missing for -name " << std::endl;
					return EXIT_FAILURE;
				}
				istr.clear();
				istr.str(argv[i+1]);
				istr >> name;
				i++;
				continue;
			} 
			if (arg=="-league") {
				if (i==argc-1) {
					std::cerr << "Argument missing for -league " << std::endl;
					return EXIT_FAILURE;
				}
				istr.clear();
				istr.str(argv[i+1]);
				istr >> league;
				i++;
				continue;
			} 
			if (arg=="-width") {
				if (i==argc-1) {
					std::cerr << "Argument missing for -width " << std::endl;
					return EXIT_FAILURE;
				}
				istr.clear();
				istr.str(argv[i+1]);
				istr >> w;
				i++;
				continue;
			} 
			if (arg=="-height") {
				if (i==argc-1) {
					std::cerr << "Argument missing for -height " << std::endl;
					return EXIT_FAILURE;
				}
				istr.clear();
				istr.str(argv[i+1]);
				istr >> h;
				i++;
				continue;
			} 
			if (arg=="-dir") {
				if (i==argc-1) {
					std::cerr << "Argument missing for -dir " << std::endl;
					return EXIT_FAILURE;
				}
				dir=argv[i+1];
				i++;
				continue;
			}
			std::cerr << "Usage: " << argv[0] << " [ -width xx ] [ -height xx ] [ -fullscreen ] [ -offline ] [ -noaudio ] [ -name name ] [ -league id ] [ -dir datadir ]" << std::endl;
			if (arg!="-help") {
				std::cerr << "Unrecognized argument " << arg << std::endl;
				return EXIT_FAILURE;
			}
			return EXIT_SUCCESS;
		}
	}

	PRJID::Config::set_league(league);
	PRJID::Config::set_name(name);
	PRJID::Config::set_offline(offline);

	int r;
	r=PRJID::main(w,h,fullscreen,audio,dir);

	if (r==EXIT_SUCCESS) {
		std::cout << std::endl << "   Thank you for playing " << FULLNAME << "." << std::endl;
	}
	return r;
}
