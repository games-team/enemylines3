#ifndef __el3__shot_h
#define __el3__shot_h

#include <iostream>
#include <vector>

#include "coordinate.h"

namespace el3 {

class Entity;

class Shot {
	unsigned int timestamp;
	void inner_draw();
	C3f dist;
	C3f from;
	C3f to;
	std::vector <C3f> positions;
	unsigned int lifetimepercent();
public:
	Shot();
	Shot(C3f f,C3f t);
	void init();
	void draw();

	static void add(C3f p,C3f to);
	static void tick_all(int ticks);
	static void draw_all();
	static void clear();
};


} //namespace

#endif
