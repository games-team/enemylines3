#ifndef __el3__sphere_h
#define __el3__sphere_h

#include <iostream>

namespace el3 {


class Sphere {

public:

	static void draw();
	static void dldraw();

};

std::ostream& operator<<(std::ostream&s, Sphere);

} //namespace

#endif
