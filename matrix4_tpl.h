
#include "SDL_opengl.h"


namespace C {


typedef enum {
   RIGHT   = 0,
   LEFT    = 1,
   BOTTOM  = 2,
   TOP     = 3,
   BACK    = 4,
   FRONT   = 5,
   SLAST =6
} side;


}

template <class T>class Matrix4_tpl {
public:
	T m[4*4];

	Matrix4_tpl <T>() {
		unsigned int i; 
		for (i=0;i<16;i++) {
			m[i]=0;
		}
	}


	Matrix4_tpl <T> operator*=(const Matrix4_tpl <T> &b) {

		m[0]=	m[0]*b.m[0]	+	m[1]*b.m[4]	+	m[2]*b.m[8]	 +	 m[3]*b.m[12];
		m[1]=	m[0]*b.m[1]	+	m[1]*b.m[5]	+	m[2]*b.m[9]	 +	 m[3]*b.m[13];			
		m[2]=	m[0]*b.m[2]	+	m[1]*b.m[6]	+	m[2]*b.m[10] +	 m[3]*b.m[14];			
		m[3]=	m[0]*b.m[3]	+	m[1]*b.m[7]	+	m[2]*b.m[11] +	 m[3]*b.m[15];			

		m[4]=	m[4]*b.m[0]	+	m[5]*b.m[4]	+	m[6]*b.m[8]	 +	 m[7]*b.m[12];			
		m[5]=	m[4]*b.m[1]	+	m[5]*b.m[5]	+	m[6]*b.m[9]	 +	 m[7]*b.m[13];			
		m[6]=	m[4]*b.m[2]	+	m[5]*b.m[6]	+	m[6]*b.m[10] +	 m[7]*b.m[14];			
		m[7]=	m[4]*b.m[3]	+	m[5]*b.m[7]	+	m[6]*b.m[11] +	 m[7]*b.m[15];			

		m[8] = m[8]*b.m[0]	+ m[9]*b.m[4] + m[10]*b.m[8]  + m[11]*b.m[12];			
		m[9] = m[8]*b.m[1]	+ m[9]*b.m[5] + m[10]*b.m[9]  + m[11]*b.m[13];			
		m[10]= m[8]*b.m[2]	+ m[9]*b.m[6] + m[10]*b.m[10] + m[11]*b.m[14];			
		m[11]= m[8]*b.m[3]	+ m[9]*b.m[7] + m[10]*b.m[11] + m[11]*b.m[15];			


		m[12]=m[12]*b.m[0] + m[13]*b.m[4] + m[14]*b.m[8]  + m[15]*b.m[12];			
		m[13]=m[12]*b.m[1] + m[13]*b.m[5] + m[14]*b.m[9]  + m[15]*b.m[13];			
		m[14]=m[12]*b.m[2] + m[13]*b.m[6] + m[14]*b.m[10] + m[15]*b.m[14];			
		m[15]=m[12]*b.m[3] + m[13]*b.m[7] + m[14]*b.m[11] + m[15]*b.m[15];
		return (*this);
	}

	void concatenate (const Matrix4_tpl <T> &b) {
    int i;
    double mb00, mb01, mb02, mb03,
           mb10, mb11, mb12, mb13,
           mb20, mb21, mb22, mb23,
           mb30, mb31, mb32, mb33;
    double mai0, mai1, mai2, mai3;

    mb00 = b.m[0];  mb01 = b.m[1];
    mb02 = b.m[2];  mb03 = b.m[3];
    mb10 = b.m[4];  mb11 = b.m[5];
    mb12 = b.m[6];  mb13 = b.m[7];
    mb20 = b.m[8];  mb21 = b.m[9];
    mb22 = b.m[10];  mb23 = b.m[11];
    mb30 = b.m[12];  mb31 = b.m[13];
    mb32 = b.m[14];  mb33 = b.m[15];

    for (i = 0; i < 4; i++) {
        mai0 = m[i*4+0];  mai1 = m[i*4+1];
       mai2 = m[i*4+2];  mai3 = m[i*4+3];

        m[i*4+0] = mai0 * mb00 + mai1 * mb10 + mai2 * mb20 + mai3 * mb30;
        m[i*4+1] = mai0 * mb01 + mai1 * mb11 + mai2 * mb21 + mai3 * mb31;
        m[i*4+2] = mai0 * mb02 + mai1 * mb12 + mai2 * mb22 + mai3 * mb32;
        m[i*4+3] = mai0 * mb03 + mai1 * mb13 + mai2 * mb23 + mai3 * mb33;
    }
}






	void glget(GLenum name) {
		glGetFloatv(name,m);
	}

	C4_tpl <T> extract_plane(C::side side) {
		C4_tpl <T> pl;
		switch (side) {
			case C::RIGHT: 
				pl.x=m[3]-m[0]; pl.y=m[7]-m[4]; pl.z=m[11]-m[8]; pl.w=m[15]-m[12];
				return pl;
			case C::LEFT:
				pl.x=m[3]+m[0]; pl.y=m[7]+m[4]; pl.z=m[11]+m[8]; pl.w=m[15]+m[12];
				return pl;
			case C::BOTTOM: 
				pl.x=m[3]+m[1]; pl.y=m[7]+m[5]; pl.z=m[11]+m[9]; pl.w=m[15]+m[13];
				return pl;
			case C::TOP:
				pl.x=m[3]-m[1]; pl.y=m[7]-m[5]; pl.z=m[11]-m[9]; pl.w=m[15]-m[13];
				return pl;
			case C::BACK: 
				pl.x=m[3]-m[2]; pl.y=m[7]-m[6]; pl.z=m[11]-m[10]; pl.w=m[15]-m[14];
				return pl;
			case C::FRONT:
				pl.x=m[3]+m[2]; pl.y=m[7]+m[6]; pl.z=m[11]+m[10]; pl.w=m[15]+m[14];
				return pl;
			default:
				return pl;
		}
	}


};

template <class T> std::ostream& operator<< (std::ostream& s, Matrix4_tpl <T>& v) {
	for (unsigned int i=0;i<16;i++) {
		s << v.m[i] << " ";
		if ((i+1)%4==0) s << std::endl;
	}

	return s ;
}
