#ifndef __font_ttf_h
#define __font_ttf_h

#include <iostream>

#include "coordinate.h"

class Font_ttf{
public:
	static unsigned int gen_dl(std::string fontname,unsigned int size,C3 col=C3(255,255,255));
	static int dx();
	static int dy();
};



#endif
