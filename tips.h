#ifndef __el3__tips_h
#define __el3__tips_h

#include <iostream>

namespace el3 {


class Tips {

public:

	static void roll();
	static void cycle();
	static void draw();

};


} //namespace

#endif
