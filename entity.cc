#include "entity.h"

#include <cmath>
#include "SDL_opengl.h"

#include "random.h"
#include "util.h"
#include "container.h"
#include "game.h"
#include "config.h"
#include "sphere.h"
#include "audio.h"

#include "models/all.h"

namespace el3 {


Entity::Entity() {
	init();
}

void Entity::init() {
	container_=NULL;
	jumping=0;
	myticks=0;
	rot.y=180.0f;
	pos.x=20;
	pos.z=20;
	type=ET_NONE;
	remove=false;
	exploding=0;
	speed=Tweak::f_player_speed();

	m_forward=false;
	m_backward=false;
	m_left=false;
	m_right=false;
}



void Entity::set_type(e_entitytype t) {
	type=t;
}
e_entitytype Entity::get_type() {
	return type;
}

void Entity::hit() {
	if (!isactive()) return;
	explode();
}

void Entity::explode() {
	exploding=Tweak::i_gfx_explosionduration();
	Audio::play(AS_EXPLOSION);
}


void Entity::draw() {
	if (remove) return;

	glPushMatrix();
	glTranslatef(pos.x,pos.y,pos.z);

	glRotatef(rot.x,-90.0f,0.0f,0.0f);
	glRotatef(90-rot.y,0.0f,-90.0f,0.0f);
	glRotatef(rot.z,0.0f,0.0f,-90.0f);


	if (exploding>0) {
		int edist=Tweak::i_gfx_explosionduration()-exploding;
		
		glColor3f(1.0f,0.0f,0.0f);
		float size=0.2f+0.001f*edist;
		glScalef(size,size,size);
		glEnable(GL_COLOR_MATERIAL);
		Sphere::dldraw();
		glDisable(GL_COLOR_MATERIAL);
		glPopMatrix();

		return;
	}


	
	if (type==ET_MONSTER||type==ET_MONSTER2) {

		if (pos.y<Tweak::f_robot_miny()) {
			float rz;
			int c=1+myticks/Tweak::i_robot_jig()%60;

			if (c>30) {
				rz=static_cast<float>((c-30)-15);
			} else {
				rz=static_cast<float>((30-c)-15);
			}


			glRotatef(rz,0.0f,0.0f,90.0f);
		}
		models::robot::dldraw();

	} else if (type==ET_KEY1) {
		rot.y+=0.51f;
		models::key::dldraw();
	} else if (type>=ET_EXTRA_LIFE&&type<=ET_EXTRA_POISON) {
		glEnable(GL_COLOR_MATERIAL);
		switch (static_cast<e_entitytype>(type)) {
			case ET_EXTRA_LIFE: glColor3ub(0,250,0); break;
			case ET_EXTRA_JUMP: glColor3ub(250,0,250); break;
			case ET_EXTRA_LASER: glColor3ub(0,250,250); break;
			case ET_EXTRA_SCORE: glColor3ub(250,250,250); break;
			case ET_EXTRA_POISON: glColor3ub(25,25,25); break;
			default:
				break;
		}
		glScalef(0.2f,0.2f,0.2f);
		Sphere::dldraw();
		glDisable(GL_COLOR_MATERIAL);
	} else if (type>=ET_SUPERCHARGE_LIFE&&type<=ET_SUPERCHARGE_LASER) {
		glEnable(GL_COLOR_MATERIAL);
		switch (static_cast<e_entitytype>(type)) {
			case ET_SUPERCHARGE_LIFE: glColor3ub(0,250,0); break;
			case ET_SUPERCHARGE_JUMP: glColor3ub(250,0,250); break;
			case ET_SUPERCHARGE_LASER: glColor3ub(0,250,250); break;
			default: break;
		}
		models::pill::dldraw();
		glDisable(GL_COLOR_MATERIAL);
	} else {
		glEnable(GL_COLOR_MATERIAL);
		switch (static_cast<e_entitytype>(type)) {
			case ET_SKIP_TIME: glColor3ub(0,250,0); break;
			case ET_SKIP_KEYS: glColor3ub(250,0,250); break;
			case ET_SKIP_KILLS: glColor3ub(0,250,250); break;
			case ET_SKIP_DEFEND: glColor3ub(250,0,0); break;
			default: break;
		}
		models::skip::dldraw();
		glDisable(GL_COLOR_MATERIAL);



	}

	glPopMatrix();
}



void Entity::act(unsigned int ticks) {
	if (!isactive()) return;

	float dist;
	if (type>ET_FALLING) {
		dist=pos.dist(game_->get_player()->pos);
		if (dist>1.0f) {
			return;
		}
		game_->action(A_PICKED,static_cast<int>(type));
		remove=true;
		return;
	}
	if (pos.y>Tweak::f_robot_miny()) return;

	if (mdest!=NULL) {
		dest=mdest->pos;
		if (Random::sget(Tweak::i_robot_lookodds())==0) lookatdest();
	}

	dist=pos.dist(dest);

	if (dist<5.0f) {
		if (Random::sget(Tweak::i_robot_lookodds_close())==0) lookatdest();
	}

	if (dist<1.0f) {
		if (mdest!=NULL) {
			game_->action(A_DAMAGE);
			explode();
			return;
		}
		new_dest();
		return;
	}

	float diff;
	diff=destroty-rot.y;
	if (fabs(diff)>1.0f) {
		float step=0.8f;
		if (destroty<rot.y) {
			rot.y-=step;
		} else {
			rot.y+=step;
		} 
		speed=Tweak::f_robot_speed_turning();
		mark_move_forward();
		return;
	} 
	speed=Tweak::f_robot_speed();
	mark_move_forward();
}

bool Entity::trymove(C3f d) {
	if (trymove_single(d)) return true;

	if (trymove_single(C3f(d.x,0,d.z))) return true;

	if (type==ET_PLAYER) if (d.x>d.z) {
		if (trymove_single(C3f(d.x,d.y,0))) return true;
		if (trymove_single(C3f(0,d.y,d.z))) return true;
	} else {
		if (trymove_single(C3f(0,d.y,d.z))) return true;
		if (trymove_single(C3f(d.x,d.y,0))) return true;
	}

	C3 sc=collision;
	
	if (trymove_single(C3f(0,d.y,0))) return true;
	collision=sc;

	if (type==ET_PLAYER) if (d.z>d.x) {
		if (trymove_single(C3f(0,0,d.z))) return true;
		if (trymove_single(C3f(d.x,0,0))) return true;
	} else {
		if (trymove_single(C3f(d.x,0,0))) return true;
		if (trymove_single(C3f(0,0,d.z))) return true;
	}

	return false;
}
bool Entity::trymove_single(C3f d) {

	C3f nextpos;
	nextpos=pos+d;

	if (!game_->isvalid(nextpos,&collision)) { 
		return false; 
	}

	pos+=d;
	snap();
	return true;
}

void Entity::tick(unsigned int ticks) {
	myticks+=ticks;

	if (exploding>0) {
		if (exploding<=ticks) { exploding=0; remove=true; return;}
		exploding-=ticks;
	}

	float fticks=static_cast<float>(ticks);
	if (m_forward) { move_forward(fticks*speed); }
	if (m_backward) { move_backward(fticks*speed); }
	if (m_left) { move_left(fticks*speed); }
	if (m_right) { move_right(fticks*speed); }

	m_forward=false;
	m_backward=false;
	m_left=false;
	m_right=false;

		
	if (jumping>0) {
		if (jumping<=ticks) { jumping=0; } else { jumping-=ticks; }
		dir.y+=Tweak::f_phys_fallconst()*fticks;
	} else {
		if (type<ET_FLOATING) dir.y-=Tweak::f_phys_fallconst()*fticks;
	}

	if (trymove(dir)) { dir=C3f(); return; }

	if (type==ET_MONSTER&&Random::sget(Tweak::i_robot_destroyodds())==0) {
		game_->action(A_DESTROY,collision.x,collision.z);
	}
	dir=C3f();
}


void Entity::lookatdest() {
	destroty=destdegree(pos,dest);
}

void Entity::new_dest() {
	lastnewdest=myticks;

	mdest=game_->get_player();
	dest=mdest->pos;

	lookatdest();
}

bool Entity::snap() {
	bool coll=false;
	if (pos.x<0) {pos.x=0; coll=true; }
	if (pos.z<0) {pos.z=0; coll=true; }
	if (pos.x>40) {pos.x=40; coll=true; }
	if (pos.z>40) {pos.z=40; coll=true; }
	if (pos.y<0) { pos.y=0; coll=true;}
	return coll;
}


bool Entity::isactive() {
	if (remove) return false;
	if (exploding>0) return false;
	return true;
}


void Entity::move_direction(C3f r,float mod) {
	C3f d;
	d=r.rotforward();

	d.z*=-1;
	dir+=d*(mod*0.01);
}


void Entity::jump() {
	jumping=Tweak::i_player_jump();
}

C3f Entity::gunpos() {
	C3f r;
	r=pos;

	C3f mrot;
	C3f mov;

	mrot=C3f(0.0f,rot.y-90.0f,0.0f);
	mov=mrot.rotforward();
	mov.z*=-1;
	mov*=Tweak::f_scale_shotpos_right();
	r+=mov;

	mrot=C3f(0.0f,rot.y,0.0f);
	mov=mrot.rotforward();
	mov.z*=-1;
	mov*=Tweak::f_scale_shotpos_forward();
	r+=mov;

	mrot=C3f(rot.x+90,0,0);
	mov=mrot.rotforward();
	mov.z*=-1;
	mov*=Tweak::f_scale_shotpos_up();
	r+=mov;

	r.y+=Tweak::f_shotpos_y();

	return r;
}

void Entity::move_forward(float mod) {
	move_direction(C3f(0.0f,rot.y,0.0f),mod);
}
void Entity::move_backward(float mod) {
	move_direction(C3f(0.0f,rot.y,0.0f),-1.0f*mod);
}

void Entity::move_left(float mod) {
	move_direction(C3f(0.0f,rot.y+90.0f,0.0f),mod);
}
void Entity::move_right(float mod) {
	move_direction(C3f(0.0f,rot.y+90.0f,0.0f),-1.0f*mod);
}



void Entity::mark_move_right() { m_right=true; }
void Entity::mark_move_left() { m_left=true; }
void Entity::mark_move_forward() { m_forward=true; }
void Entity::mark_move_backward() { m_backward=true; }

void Entity::turn_right() {
	rot.y-=0.1f;
}

void Entity::turn_left() {
	rot.y+=0.1f;
}
void Entity::turn_up() {
	rot.x+=0.1f;
	if (rot.x>60.0f) rot.x=60.0f;
	if (rot.x<-70.0f) rot.x=-70.0f;
}
void Entity::turn_down() {
	rot.x-=0.1f;
	if (rot.x>60.0f) rot.x=60.0f;
	if (rot.x<-70.0f) rot.x=-70.0f;
}
void Entity::turn(int x,int y) {
	rot.y-=(static_cast<float>(x))/5.0f;
	rot.x+=((static_cast<float>(y))/5.0f)*Config::mouse_reverse();
	if (rot.x>70.0f) rot.x=70.0f;
	if (rot.x<-70.0f) rot.x=-70.0f;
}

} // namespace
