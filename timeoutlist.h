#ifndef __el3__timeoutlist_h
#define __el3__timeoutlist_h

#include <iostream>
#include <list>
#include <vector>

namespace el3 {
template <class T> class Item {
	public:
	unsigned int timestamp;
	T data;
};


template <class T> class Timeoutlist {
	std::vector < Item <T> > items;
	typename std::vector< Item <T> >::iterator pit;
public:
	Timeoutlist() {
		pit=items.end();
	}


	void add(T item,unsigned int timeout) {
		Item <T> i;
		i.data=item;
		i.timestamp=timeout;
		items.push_back(i);
		pit=items.end();
	}

	void timeout(unsigned int ticks) {
   	typename std::vector< Item <T> >::iterator it;

		bool found;
		do {
			found=false;
			while (iterate()) {
				if ((*pit).timestamp>ticks) continue;
				items.erase(pit);
			}
			pit=items.end();
		} while (found);
		pit=items.end();
	}

   bool iterate() {
      if (pit==items.end()) {
         pit=items.begin(); 
      	if (pit==items.end()) return false;
			return true;
      }
      pit++;
      if (pit==items.end()) return false;
      return true;
   }



	T get() {
		return (*pit).data;
	}



};

} //namespace

#endif
