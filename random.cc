#include "random.h"

#include <iostream>
#include <cstdlib>


namespace el3 {

static bool seeded=false;


void Random::sseed() {
	sseed(time(NULL));
}

bool Random::boolean() {
	if (sget(2)==1) return true;
	return false;
}


void Random::sseed(unsigned int s) {
	srand(s);
	seeded=true;
}

std::vector <int> Random::randomize(std::vector <int> a) {
	int r;

	for (unsigned int i=0;i<a.size()/2;i++) {
		r=1+get((a[i]/2));

		a[i]-=r;
		a[i+a.size()/2]+=r;
	}
	return a;
}


unsigned int Random::get() {
	return rand();
}

unsigned int Random::get(unsigned int below) {
	if (below<=1) return 0;
	return get()%below;
}

float Random::sgetf() {

	float i;

	i=(float)sget(100000);
	i/=100000;
	return i;
}


bool Random::sboolean() {
	if (sget(2)==1) return true;
	return false;
}

int Random::ssign() {
	if (sboolean()) return -1;
	return 1;
}


int Random::sget() {
	return rand();
}

int Random::sget(unsigned int below) {
	return sget()%below;
}




Random *sirg=NULL;

Random *Random::instance() {
	if (sirg==NULL) {
		sirg=new Random();
	}
	return sirg;
}


}
