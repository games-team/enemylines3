#include "map.h"

namespace el3 {

Map::Map() {
	init();
}

void Map::init() {
	int x,z;


	for (x=0;x<MX;x++) {
		for (z=0;z<MZ;z++) {
			data[x][z]=TT_LEVEL1;
		}
	}

}


unsigned int Map::dx() { return MX; }
unsigned int Map::dy() { return 0; }
unsigned int Map::dz() { return MZ; }


bool Map::inside(C3 c) {
	if (c.x<0) return false;
	if (c.z<0) return false;
	if (c.x>=dx()) return false;
	if (c.z>=dz()) return false;
	return true;
}
void Map::clear(e_tiletype t) {
	C3 p;
	for (p.x=0;p.x<dx();p.x++) {
		for (p.z=0;p.z<dz();p.z++) {
			set(p,t);

		}
	}

}

void Map::set(C3 c,e_tiletype t) {
	if (!inside(c)) return;

	data[c.x][c.z]=t;
}
	
e_tiletype Map::get(C3 c) {
	if (!inside(c)) return TT_LEVEL0;

	return data[c.x][c.z];
}
std::ostream& operator<<(std::ostream&s, Map sk) {

	unsigned int x,z;
	for (x=0;x<sk.dx();x++) {
		for (z=0;z<sk.dz();z++) {
			s << sk.get(C3(x,0,z));
		}
		s << std::endl;
	}
	return s;
}
void Map::destroy(C3 p) {
	e_tiletype t;
	t=get(p);
	if (t>TT_LEVEL1) {
		set(p,static_cast<e_tiletype>(static_cast<int>(t)-1));
	}
}

void Map::lower(C3 p) {

   el3::e_tiletype t;
   t=get(p);
   int i=static_cast<int>(t);
   if (t>el3::TT_LEVEL1) i--;

   t=static_cast<el3::e_tiletype>(i);

   set(p,t);
}
void Map::raise(C3 p) {
   el3::e_tiletype t;
   t=get(p);
   int i=static_cast<int>(t);
   if (t<el3::TT_LEVEL5) i++;

   t=static_cast<el3::e_tiletype>(i);

   set(p,t);
}

int Map::typetoheight(e_tiletype t) {
   switch (t) {
      case TT_LEVEL0: return 0;
      case TT_LEVEL1: return 1;
      case TT_LEVEL2: return 2;
      case TT_LEVEL3: return 3;
      case TT_LEVEL4: return 4;
      case TT_LEVEL5: return 5;
      case TT_DEFEND: return 2;
      default: return 0;
   }
}
static const int maxheight=5;


C3 toint(C3f p) {
	p.y-=0.2f;

	C3 pi;
	pi=p.toint();
	return pi;
}
bool Map::isvalid(e_tiletype t,float y) {
	if (y<0) return false;
	if (y>maxheight) return true;
	if (t==TT_LEVEL0) return true;
	if (t==TT_LEVEL1) return true;
	if (t==TT_LEVEL2||t==TT_DEFEND) { if (y>1.00f) return true; return false; }
	if (t==TT_LEVEL3) { bool r; r=false; if (y>2.00f) r=true; return r; }
	if (t==TT_LEVEL4) { bool r; r=false; if (y>3.00f) r=true; return r; }
	if (t==TT_LEVEL5) { bool r; r=false; if (y>4.00f) r=true; return r; }
	return false;
}

bool Map::invalidahead(C3f p,C3 *ret) {
	/*if (p.y>maxheight) return true;

	C3 pi=p.toint();
	if (!isvalid(pi,p.y)) {
		ret->x=pi.x; ret->y=pi.y; ret->z=pi.z;
		return false;
	}
	C3f r=p;

	r.x-=pi.x; r.y-=pi.y; r.z-=pi.z; 
	static const float dist=0.45f;
	static const float adist=1.0f-dist;

	C3 n=pi;
	C3 n2=pi;
	if (r.x<dist) { 
		n.x--;  n2.x--; 
		if (!isvalid(n2,p.y)) {
			ret->x=n2.x; ret->y=n2.y; ret->z=n2.z;
			return false; 
		}
		n2=pi;
	} 
	if (r.x>adist) { 
		n.x++;  n2.x++; 
		if (!isvalid(n2,p.y)) {
			ret->x=n2.x; ret->y=n2.y; ret->z=n2.z;
			return false; 
		}
		n2=pi;
	} 
	if (r.z<dist) { 
		n.z--;  n2.z--; 
		if (!isvalid(n2,p.y)) {
			ret->x=n2.x; ret->y=n2.y; ret->z=n2.z;
			return false; 
		}
		n2=pi;
	} 
	if (r.z>adist) { 
		n.z++;  n2.z++; 
		if (!isvalid(n2,p.y)) {
			ret->x=n2.x; ret->y=n2.y; ret->z=n2.z;
			return false; 
		}
		n2=pi;
	} 

	if (!isvalid(n,p.y)) {
		ret->x=n.x; ret->y=n.y; ret->z=n.z;
		return false;
	}*/

	return true;
}

bool Map::isvalid(C3 p,float y,C3 *ret) {
	e_tiletype t;
	t=get(p);
	*ret=p;
	return isvalid(t,y);
}

bool Map::isvalid(C3f p,C3 *ret) {
	if (p.y<0) return false;
	if (p.y>maxheight) return true;

	C3 pi=toint(p);

	
	if (!isvalid(pi,p.y,ret)) return false;

	C3f r=p;

	r.x-=pi.x;
	r.y-=pi.y;
	r.z-=pi.z;

	static const float dist=0.45f;
	static const float adist=1.0f-dist;

	C3 n=pi;
	C3 n2=pi;
	if (r.x<dist) { n.x--;  n2.x--; if (!isvalid(n2,p.y,ret)) return false; n2=pi;} 
	if (r.x>adist) { n.x++;  n2.x++; if (!isvalid(n2,p.y,ret)) return false; n2=pi;} 
	if (r.z<dist) { n.z--;  n2.z--; if (!isvalid(n2,p.y,ret)) return false; n2=pi;} 
	if (r.z>adist) { n.z++;  n2.z++; if (!isvalid(n2,p.y,ret)) return false; n2=pi;} 

	if (!isvalid(n,p.y,ret)) return false;


	return true;
}

} //namespace
