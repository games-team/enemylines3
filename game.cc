#include <sstream>
#include "SDL_opengl.h"

#include "config.h"
#include "game.h"
#include "util.h"
#include "entity.h"
#include "shot.h"
#include "random.h"
#include "map.h"
#include "mapswitch.h"
#include "font_ogl.h"
#include "help.h"
#include "skybox.h"
#include "tile.h"
#include "audio.h"
#include "timeoutlist.h"
#include "hiscore.h"

#include "generators/generators.h"
#include "tweak/tweak.h"

#include "models/all.h"

namespace el3 {


typedef enum e_ingamemenuids {
	IG_POP,
   IG_NONE,
   IG_BACK,
   IG_QUIT
};


static const unsigned int jetpackaudiointerval=1300;

static const unsigned int showstartmessage=20000;
static const unsigned int endclick=200;


Game::~Game() {
	if (map_!=NULL) delete map_;
}

Game::Game(Game *g,E_Diffi diffi) {

	difficulty=diffi;

	container_.set_game(this);
	player_=container_.create(ET_PLAYER);


	score=Score();
	score.dim(C3(380,14),10);
	score.label("Score:",C3(320,14));

	level=Level();
	level.dim(C3(380,0),10);
	level.label("Level:",C3(320,0));

	skipping=LT_NONE;
	if (g) {
		level.set_current(g->get_level());
		level.gain(1);
		score.set_current(g->get_score());
		difficulty=g->get_difficulty();
		skipping=g->get_skipping();
	} else {
	}

	map_=new Map();
	map_->clear(TT_LEVEL2);
	Mapswitch ms(map_);


	int mg;
	if (level.get()<5) {
		mg=Random::sget(3);
	} else {
		mg=Random::sget(7);
	}
	switch (mg) {
		default:
		case 0: 
			intreg1(&ms,Random::instance()); 
			randomize_height(&ms,Random::instance());
			break;
		case 1: 
			pyr(&ms,Random::instance()); 
			break;
		case 2: 
			lab(&ms,Random::instance()); 
			randomize_height(&ms,Random::instance());
			break;
		case 3: 
			hole(&ms,Random::instance()); 
			randomize_height(&ms,Random::instance());
			break;
		case 4: 
			stamp(&ms,Random::instance()); 
			randomize_height(&ms,Random::instance());
			if (Random::sget(3)==0) invert(&ms,Random::instance());
			break;
		case 5: 
			intreg1(&ms,Random::instance()); 
			randomize_height(&ms,Random::instance());
			invert(&ms,Random::instance());
			break;
		case 6: 
			lab(&ms,Random::instance()); 
			randomize_height(&ms,Random::instance());
			invert(&ms,Random::instance());
			break;
	}


	map_->set(C3(20,0,20),TT_LEVEL1);
	map_->set(C3(21,0,20),TT_LEVEL1);
	map_->set(C3(20,0,21),TT_LEVEL1);
	map_->set(C3(19,0,19),TT_LEVEL1);
	map_->set(C3(19,0,20),TT_LEVEL1);
	map_->set(C3(20,0,19),TT_LEVEL1);



	supercharge=Supercharge();
	supercharge.set_length(SC_LASER,Tweak::i_supercharge_laser_duration());
	supercharge.set_length(SC_LIFE,Tweak::i_supercharge_life_duration());
	supercharge.set_length(SC_JUMP,Tweak::i_supercharge_jump_duration());
	supercharge.set_length(SC_WIRE,Tweak::i_supercharge_neg_duration());
	supercharge.set_length(SC_FREEZE,Tweak::i_supercharge_neg_duration());
	supercharge.set_length(SC_NOLASER,Tweak::i_supercharge_neg_duration());

	life_energy_=Energy(Tweak::i_balance_maxlife());
	life_energy_.dim(
		C3(0,250,0),
		C3f(1,4),
		C3f(60,6)
	);
	life_energy_.warn(true);
	life_energy_.label("health:",C3());

	laser_energy_=Energy(Tweak::i_balance_maxlaser());
	laser_energy_.dim(
		C3(0,250,250),
		C3f(1,4),
		C3f(60,20)
	);
	laser_energy_.label("laser:",C3(0,14));

	jump_energy_=Energy(Tweak::i_balance_maxjump());
	jump_energy_.dim(
		C3(250,0,250),
		C3f(1,4),
		C3f(60,34)
	);
	jump_energy_.label("jetpack:",C3(0,28));



	lastjetpackaudio=0;

	regen_interval=Interval(Tweak::i_balance_regeninterval());
	wave_interval=Interval(Tweak::i_balance_waveinterval());
	wave_interval.tick(Tweak::i_balance_waveinterval()/2);
	act_interval=Interval(Tweak::i_balance_actinterval());

	ticks_=0;
	menu_active=false;

	menu=Menu(C3(250,100));

   menu.add(Menuitem("Resume game",IG_BACK));
   menu.add(Menuitem("End game",IG_QUIT));

	state=GS_PLAYING;

	do {
		E_LevelType types[]={LT_KEYS,LT_KILLS,LT_TIME,LT_DEFEND};
		unsigned int t=Random::sget(4);
		type=types[t];
		switch (type) {
			case LT_KEYS:
				goal_keys=Goal_Keys(
					(Tweak::i_balance_ltkeys_base()+
					level.get()*Tweak::i_balance_ltkeys_fact())/100
				);
				goal_keys.dim(C3(380,28),10);
				goal_keys.label("Keys:",C3(320,28));
				break;
			case LT_KILLS:
				goal_kills=Goal_Kills(
					Tweak::i_balance_ltkills_base()+
					level.get()*Tweak::i_balance_ltkills_fact()
				);
				goal_kills.dim(C3(380,28),10);
				goal_kills.label("Kills:",C3(320,28));
				break;
			case LT_TIME:

				goal_time=Goal_Survive(
					(Tweak::i_balance_lttime_base()+
					level.get()*Tweak::i_balance_lttime_fact())*1000
				);
				goal_time.dim(C3(380,28),10);
				goal_time.label("Survive for:",C3(320,28));
				break;
			case LT_DEFEND:
				goal_defend=Goal_Survive(
					(Tweak::i_balance_ltdefend_base()+
					level.get()*Tweak::i_balance_ltdefend_fact())*1000
				);
				goal_defend.dim(C3(380,28),10);
				goal_defend.label("Defend for:",C3(320,28));
				break;
			default:
				break;
		}
	} while (skipping==type);

	

	if (level.get()>3) {
		for (unsigned int i=0;i<level.get()/3;i++) {
			if (Random::sget(3)!=0) continue;
			switch (level.get()%3) {
				case 0: laser_energy_.reduce(Tweak::i_balance_laser_randreduction()); break;
				case 1: life_energy_.reduce(Tweak::i_balance_life_randreduction()); break;
				case 2: jump_energy_.reduce(Tweak::i_balance_jump_randreduction()); break;
			}
		}
	}
	if (life_energy_.empty()) life_energy_.set_current(Tweak::i_balance_minstartlife());

	if (level.get()==1) {
		action(A_ADDSKIP);
	}

	if (type==LT_DEFEND) {
		C3 p;
		for (p.x=-4;p.x<=4;p.x++) {
			for (p.z=-4;p.z<=4;p.z++) {
				e_tiletype t;
				t=TT_LEVEL1;
				if (abs(p.x)==2&&p.z>-3&&p.z<3) t=TT_LEVEL4;
				if (abs(p.z)==2&&p.x>-3&&p.x<3) t=TT_LEVEL4;
				if (p.x==0&&p.z==0) t=TT_DEFEND;
				map_->set(C3(20+p.x,0,20+p.z),t);
			}
		}
		player_->pos.y=5;
	}
	seed=level.get()/3;
	Skybox::gen_dl(level.get());

	cap();
}

bool Game::isactive() {
	return !life_energy_.empty();
}

int Game::action(E_Action id,int x,int y) {
	if (!isactive()&&id!=A_TURN) { return 0; }
	if (state!=GS_PLAYING&&id!=A_TURN) return 0;
	Entity *n=NULL;
	Entity *selected=NULL;
	switch (id) {
		case A_FORWARD: player_->mark_move_forward(); break;
		case A_BACKWARD: player_->mark_move_backward(); break;
		case A_LEFT: player_->mark_move_left(); break;
		case A_RIGHT: player_->mark_move_right(); break;

		case A_TURNLEFT: player_->turn_left(); break;
		case A_TURNRIGHT: player_->turn_right(); break;

		case A_TURN:
			player_->turn(x,y);
			break;
		case A_JUMP:
			if (jump_energy_.empty()||supercharge.get()==SC_FREEZE) {
				Audio::play(AS_PROBLEM,AC_PROBLEM);
				break;
			}
			if (ticks_>lastjetpackaudio+jetpackaudiointerval) {
				Audio::play(AS_JETPACK,AC_JETPACK);
				lastjetpackaudio=ticks_;
			}
			player_->jump();
			if (supercharge.get()!=SC_JUMP) jump_energy_.reduce(Tweak::i_balance_jumpcost());
			break;
		case A_FIRE: {
			if (supercharge.get()==SC_NOLASER) {
				Audio::play(AS_PROBLEM,AC_PROBLEM);
				break;
			}
			if (!laser_energy_.can_afford(Tweak::i_balance_lasercost())) {
				Audio::play(AS_PROBLEM,AC_PROBLEM);
				break;
			}
			Audio::play(AS_LASER,AC_LASER);
			if (supercharge.get()!=SC_LASER) laser_energy_.reduce(Tweak::i_balance_lasercost());
			glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();
			rotrans();
			glDisable(GL_LIGHTING);
			draw_map();

			selected=container_.select(x,y);
			glEnable(GL_LIGHTING);
			C3f gunpos;
			gunpos=player_->gunpos();

			if (selected&&selected->isactive()) {
				Shot::add(gunpos,unproject(x,y));
				selected->hit();
				if (selected->get_type()==ET_MONSTER) {
					score.gain(Tweak::i_score_monsterkill());
					goal_kills.gotone();
				}
				if (selected->get_type()==ET_KEY1) {
					end_game();
				}
				break;
			}
			cap();

			Shot::add(gunpos,unproject(x,y));
			} break;
		case A_ADDENEMY: {
			if (Tweak::i_debug_dropenemies()==0) break;
			n=container_.create(ET_MONSTER);
			n->new_dest();
			start_pos(n,true);
			} break;
		case A_ADDSKIP: {
			n=container_.create(ET_SKIP_KILLS);
			start_pos(n,true);
			n=container_.create(ET_SKIP_TIME);
			start_pos(n,true);
			n=container_.create(ET_SKIP_KEYS);
			start_pos(n,true);
			n=container_.create(ET_SKIP_DEFEND);
			start_pos(n,true);
		}
		case A_ADDEXTRA: {
			n=container_.create(random_extra());	
			start_pos(n);
			} break;
		case A_ADDKEY: {
			if (!goal_keys.drop()) break;
			n=container_.create(ET_KEY1);
			start_pos(n);
			} break;
		case A_DAMAGE:
			if (supercharge.get()==SC_LIFE) break;
			goal_kills.gotone();;
			life_energy_.reduce(
				Tweak::i_balance_lifecosti()*static_cast<int>(difficulty)+
				Random::sget(Tweak::i_balance_lifecostrand())
			);
			Audio::schedule_play(120,AS_HURT,AC_HURT);
			if (life_energy_.empty()) {
				end_game();
			}
			break;
		case A_DESTROY: {
			if (type==LT_DEFEND&&x==20&&y==20) {
				if (Random::sget(3)==0) {
					end_game();
				} else {
					break;
				}
			}
			destroyed.add(C3(x,0,y),ticks_+Tweak::i_gfx_destroyedwire());
			map_->destroy(C3(x,0,y));
			} break;
		case A_PICKED:
			Audio::play(AS_PICK,AC_PICK);
			pick(static_cast<e_entitytype>(x));
			cap();
			break;
		default:
			break;
	}
	return 0;
}
void Game::cap() {
	bool won=false;
	if (state==GS_PLAYING) switch (type) {
		case LT_KILLS: if (goal_kills.finished()) { won=true; } break;
		case LT_TIME: if (goal_time.finished()) {  won=true; } break;
		case LT_DEFEND: if (goal_defend.finished()) {  won=true; } break;
		case LT_KEYS: if (goal_keys.finished()) { won=true; } break;
		default:
			break;
	}
	if (won) {
		state=GS_FINISHED; 
		end_=ticks_;
		Audio::schedule_play(1000,AS_PASSED,AC_TITLE);
		Shot::clear();
		score.gain(
			Tweak::i_score_level()
			*difficulty
		);
	}
}


void Game::pick(e_entitytype t) {
	if (t==ET_KEY1) {
		goal_keys.gotone(); 
		score.gain(Tweak::i_score_key());
		return;
	}

	switch (supercharge.get()) {
		case SC_JUMP: supercharge.charge(SC_FREEZE); return;
		case SC_LIFE: supercharge.charge(SC_WIRE); return;
		case SC_LASER: supercharge.charge(SC_NOLASER); return;
		default:
			break;
	}

	switch (t) {
		default: break;

		case ET_EXTRA_LIFE: life_energy_.gain(Tweak::i_extra_life()); break;
		case ET_EXTRA_LASER: laser_energy_.gain(Tweak::i_extra_laser()); break;
		case ET_EXTRA_JUMP: jump_energy_.gain(Tweak::i_extra_jump()); break;
		case ET_EXTRA_SCORE: score.gain(Tweak::i_score_extra()); break;
		case ET_EXTRA_POISON: 
			life_energy_.reduce(Tweak::i_extra_poison()); 
			Audio::play(AS_HURT,AC_HURT);
			break;


		case ET_SUPERCHARGE_LIFE: supercharge.charge(SC_LIFE); break;
		case ET_SUPERCHARGE_JUMP: supercharge.charge(SC_JUMP); break;
		case ET_SUPERCHARGE_LASER: supercharge.charge(SC_LASER); break;

		case ET_SKIP_KILLS:if (skipping!=LT_NONE) break;skipping=LT_KILLS;break;
		case ET_SKIP_TIME:if (skipping!=LT_NONE) break;skipping=LT_TIME;break;
		case ET_SKIP_KEYS:if (skipping!=LT_NONE) break;skipping=LT_KEYS;break;
		case ET_SKIP_DEFEND:if (skipping!=LT_NONE) break;skipping=LT_DEFEND;break;
	}
}


void Game::end_game() {

	Hiscore::report(score.get(),difficulty,level.get());
	life_energy_.set_current(0);
	Audio::schedule_play(2000,AS_FAILED,AC_TITLE);
	Shot::clear();
}

e_entitytype Game::random_extra() {
static const int drop[]={ET_EXTRA_LIFE,ET_EXTRA_JUMP,ET_EXTRA_LASER,ET_EXTRA_SCORE,ET_EXTRA_POISON};
static const int superdrop[]={ET_SUPERCHARGE_LIFE,ET_SUPERCHARGE_JUMP,ET_SUPERCHARGE_LASER};
	if (Random::sget(3)==0) {
		return static_cast<e_entitytype>(superdrop[Random::sget(3)]);
	} else {
		return static_cast<e_entitytype>(drop[Random::sget(5)]);
	}
}

void Game::start_pos(Entity *n,bool skip) {
	
	n->pos.x=static_cast<float>(Random::sget(40));
	n->pos.z=static_cast<float>(Random::sget(40));
	if (skip) {
		n->pos.y=10+static_cast<float>(Random::sget(10));
	} else {
		n->pos.y=20+static_cast<float>(Random::sget(20));
	}
	n->pos.x+=0.5f; n->pos.z+=0.5f;
}

unsigned int Game::num_enemies() {
	unsigned int e;
	e=
		Tweak::i_balance_numenemies()+
		level.get()*Tweak::i_balance_numenemies_levelfact()+
		difficulty*Tweak::i_balance_numenemies_diffifact()
	;
	if (type==LT_DEFEND) { e*=2; }
	if (type==LT_TIME) { e*=2; }
	return e;
}

void Game::tick(unsigned int ticks) {
	if (menu_active) return;
	ticks_+=ticks;
	if (state!=GS_PLAYING) return;
	if (!isactive()) return;

	regen_interval.tick(ticks);
	if (regen_interval.check()) {
		jump_energy_.gain(Tweak::i_balance_jumpregen());
		laser_energy_.gain(Tweak::i_balance_laserregen());
	}
	wave_interval.tick(ticks);
	if (wave_interval.check()) {
		for (int i=0;i<num_enemies();i++) {
			action(A_ADDENEMY);
		}
		action(A_ADDEXTRA);
		action(A_ADDEXTRA);
		if (type==LT_KEYS) {
			if (Random::sget(3)!=0) {
				action(A_ADDKEY);
			}
		}
	}
	act_interval.tick(ticks);
	if (act_interval.check()) {
		container_.act(ticks);
	}

	switch (type) {
		default: break;
		case LT_TIME: goal_time.tick(ticks); break;
		case LT_DEFEND: goal_defend.tick(ticks); break;
	}

	container_.tick(ticks);

	life_energy_.tick(ticks);
	jump_energy_.tick(ticks);
	laser_energy_.tick(ticks);
	supercharge.tick(ticks);
	Shot::tick_all(ticks);
	cap();
}


void Game::rotrans() {
	glLoadIdentity();

	glRotatef(player_->rot.x,1.0f,0.0,0.0);
	glRotatef(90-player_->rot.y,0.0,1.0f,0.0);
	glRotatef(player_->rot.z,0.0,0.0,1.0f);


	Skybox::draw();

	glTranslatef(-player_->pos.x,-player_->pos.y,-player_->pos.z);

	glTranslatef(0.0f,-0.5f,0.0f);
}


void Game::draw_hud() {

	ortho2d();


	if (isactive()&&ticks_<showstartmessage) { 
		Help::goal(type,level.get()>1); 

		if (level.get()==1) {
		if (ismouselocked()) {
			Help::mouselock_off();
		} else {
			Help::mouselock();
		}
		}
	}



	if (supercharge.get()==SC_LIFE) {life_energy_.override_color(C3(0,150,0));}
	if (supercharge.get()==SC_WIRE) {life_energy_.override_color(C3(0,50,0));}
	life_energy_.draw();

	if (supercharge.get()==SC_JUMP) {jump_energy_.override_color(C3(150,0,150));}
	if (supercharge.get()==SC_FREEZE) {jump_energy_.override_color(C3(50,0,50));}
	jump_energy_.draw();

	if (supercharge.get()==SC_LASER) {laser_energy_.override_color(C3(0,150,150));}
	if (supercharge.get()==SC_NOLASER) {laser_energy_.override_color(C3(0,50,50));}
	laser_energy_.draw();


	score.draw();
	level.draw();

	switch (type) {
		case LT_TIME: goal_time.draw(); break;
		case LT_DEFEND: goal_defend.draw(); break;
		case LT_KILLS: goal_kills.draw(); break;
		case LT_KEYS: goal_keys.draw(); break;
		default: break;
	}



	if (!isactive()) { 
		Help::gameover(); 
		Hiscore::draw();
		Shot::clear();
	} else {
		crosshair();
	}
	if (state==GS_FINISHED) { 
		Help::fin(); 
		Shot::clear();
	}
	


	ortho2d_off();
}
bool Game::check_tile(C3 i) {
	e_tiletype t;
	t=map_->get(i);

	C3f p;
	Box3f b;
	p=i.tofloat();
	int h=Map::typetoheight(t);
	for (int i=0;i<h;i++) {
		b.clear();
		b.add(C3f(p.x,p.y+i,p.z));
		b.add(C3f(p.x+1,p.y+i+1,p.z+1));
		if (frustum.test(b)) return true;
		if (frustum.test_point(p+0.5)) return true;
	}
	return false;


}
void Game::draw_map() {
	C3 i;

	for (i.z=0;i.z<static_cast<int>(map_->dz());i.z++) {
		for (i.x=0;i.x<static_cast<int>(map_->dx());i.x++) {
			if (!check_tile(i)) continue;
			glPushMatrix();
			glTranslatef(static_cast<float>(i.x),0.0f,static_cast<float>(i.z));
			Tile::draw(i,map_,seed);
			glPopMatrix();
		}
	}

	if (supercharge.get()==SC_WIRE)  return;
	destroyed.timeout(ticks_);
	while (destroyed.iterate()) {
		i=destroyed.get();

		glPushMatrix();
		glTranslatef(static_cast<float>(i.x),0.0f,static_cast<float>(i.z));
		
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		Tile::draw_above(i,map_,seed);
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
		glPopMatrix();
	}


}
void Game::draw_weapon() {
	if (!Config::show_gun()) return;
	glEnable(GL_LIGHTING);
	glClear(GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(
		Tweak::f_guntransx(),
		Tweak::f_guntransy(),
		Tweak::f_guntransz()
	);
	glRotatef(180+Tweak::f_gunroty(),0,1,0);
	models::weapon::sdraw();

//draw laser energy indicator on the weapon 
	glEnable(GL_COLOR_MATERIAL);
	unsigned char v;
	v=50+laser_energy_.percent()*2;
	glColor3ub(0,v,v);
	const float vertices[4][3]={
		{5.2123470e-2,8.5285701e-2,0.21570933},
		{6.2018618e-2,7.8857540e-2,0.21572222},
		{3.9164487e-2,6.7596472e-2,6.0273323e-2},
		{4.6897849e-2,6.2300834e-2,5.6965462e-2}
	};
	const float normals[4][3]={
		{0.53407848,0.83389828,-0.13918996},
		{0.53430453,0.83393363,-0.13810633},
		{0.51953524,0.84308496,-0.13889161},
		{0.51976271,0.84312254,-0.13780820}
	};
	glBegin(GL_POLYGON);
		glNormal3fv(normals[145-144]);
		glVertex3fv(vertices[145-144]);
		glNormal3fv(normals[147-144]);
		glVertex3fv(vertices[147-144]);
		glNormal3fv(normals[146-144]);
		glVertex3fv(vertices[146-144]);
		glNormal3fv(normals[144-144]);
		glVertex3fv(vertices[144-144]);
	glEnd();
	glDisable(GL_COLOR_MATERIAL);

	glPopMatrix();
}

void Game::draw() {
	if (menu_active) {
		ortho2d();
		menu_bg();
		Help::paused();
		menu.draw();
		Help::controls();
		Help::gameplay();
		ortho2d_off();	
		return;
	}

	glEnable(GL_LIGHTING);

	rotrans();

	if (supercharge.get()==SC_WIRE) { glPolygonMode(GL_FRONT_AND_BACK,GL_LINE); }

	frustum.changed();
	frustum.calculate_if();
	draw_map();

	container_.draw();
	if (supercharge.get()==SC_WIRE) { glPolygonMode(GL_FRONT_AND_BACK,GL_FILL); }
	
	Shot::draw_all();
	draw_weapon();
	draw_hud();
}

bool Game::invalidahead(C3f p,C3 *ret) {
	return map_->invalidahead(p,ret);
}

bool Game::isvalid(C3f p,C3 *ret) {
	return map_->isvalid(p,ret);
}


bool Game::handle_event(SDL_Event event) {
	if (menu_active) {
		e_ingamemenuids id=(e_ingamemenuids)menu.handle_event(event);
		switch (id) {
			case MI_NONE: return true; break;
			case MI_POP:
			case IG_BACK: menu_active=false; return true; break;
			case IG_QUIT: state=GS_QUIT; break;
			default: break;
		}
	} else {
		return handle_game_event(event);
	}
	return false;
}


bool Game::handle_game_event(SDL_Event event) {
	switch (event.type) {
		case SDL_QUIT: return false; break;
		case SDL_MOUSEBUTTONDOWN:
			if (state==GS_FINISHED&&ticks_>end_+endclick) {
				state=GS_NEXTLEVEL; return true;
			}
			if (ismouselocked()) {
				action(A_FIRE,Config::dx()/2,Config::dy()/2);
				break;
			}
			lockmouse();
			break;
		case SDL_MOUSEMOTION:
			if (!ismouselocked()) break;
			action(A_TURN,event.motion.xrel,event.motion.yrel);
			break;
		case SDL_KEYDOWN:
			if (state==GS_FINISHED&&ticks_>end_+endclick) {state=GS_NEXTLEVEL; return true;}

			switch (event.key.keysym.sym) {
				case SDLK_p:
				case SDLK_ESCAPE: 
					menu_active=true;
					menu.reset();
					break;
				case SDLK_l: state=GS_NEXTLEVEL; break;
				case SDLK_k: end_game(); break;
				default:
					return false;
					break;
			}
			break;

	}
	return true;
}
void Game::handle_state() {
	Uint8 *keys;
	keys = SDL_GetKeyState(NULL);


	if (ismouselocked()) {
		if (keys[SDLK_a]) {action(A_LEFT); }
		if (keys[SDLK_LEFT]) {action(A_LEFT); }
		if (keys[SDLK_d]) {action(A_RIGHT); }
		if (keys[SDLK_RIGHT]) {action(A_RIGHT); }
	} else {
		if (keys[SDLK_a]) {action(A_TURNLEFT); }
		if (keys[SDLK_LEFT]) {action(A_TURNLEFT); }
		if (keys[SDLK_d]) {action(A_TURNRIGHT); }
		if (keys[SDLK_RIGHT]) {action(A_TURNRIGHT); }
	}

	if (keys[SDLK_w]) {action(A_FORWARD); }
	if (keys[SDLK_s]) {action(A_BACKWARD); }
	if (keys[SDLK_UP]) {action(A_FORWARD); }
	if (keys[SDLK_DOWN]) {action(A_BACKWARD); }

	if (keys[SDLK_SPACE]) {action(A_JUMP); }
}

} //namespace
