#ifndef __el3__random_h
#define __el3__random_h

#include <iostream>
#include <vector>

namespace el3 {

class Random {

public:
	unsigned int get();
	unsigned int get(unsigned int below);
	unsigned int get(unsigned int above,unsigned int below);
	std::vector <int> randomize(std::vector <int>);
	bool boolean();
	

	static int sget();
	static int sget(unsigned int below);
	static float sgetf();
	static bool sboolean();
	static int ssign();

	static void sseed();
	static void sseed(unsigned int s);

	static Random *instance();



};


}


#endif
