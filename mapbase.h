#ifndef __el3__mapbase_h
#define __el3__mapbase_h

#include "coordinate.h"
#include "tiletype.h"

namespace el3 {


class Mapbase {

public:

	virtual ~Mapbase() {}

	virtual unsigned int dx()=0;
	virtual unsigned int dy()=0;
	virtual unsigned int dz()=0;

	virtual void clear(e_tiletype t)=0;

	virtual bool inside(C3 c)=0;
	
	virtual e_tiletype get(C3)=0;
	virtual void set(C3,e_tiletype)=0;

	virtual void lower(C3 p)=0;
	virtual void raise(C3 p)=0;
};


} //namespace

#endif
