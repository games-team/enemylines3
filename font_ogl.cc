#include "SDL.h"
#include "SDL_opengl.h"
#include <iostream>

#include "font_ogl.h"



class data {
public:
unsigned int dx,dy;
GLuint dls;
};

data fonts[10];
unsigned int current=0;

void Font_ogl::init_font(unsigned int f,unsigned int dl,unsigned int x,unsigned int y) {

	fonts[f].dls=dl;
	fonts[f].dx=x;
	fonts[f].dy=y;
}

void Font_ogl::use_font(unsigned int f) {
	current=f;
}



unsigned int Font_ogl::dx() { return fonts[current].dx; }
unsigned int Font_ogl::dy() { return fonts[current].dy; }

void Font_ogl::drawletter(char letter) {
	glCallList(fonts[current].dls+letter-33);
}

void Font_ogl::write(const std::string str,E_FontAnchor a) {
	write(str.c_str(),a);
}
void Font_ogl::write(const char *text,E_FontAnchor a) {
	glPushMatrix();
	if (a==FA_CENTER) {
		glTranslatef(-(float)(strlen(text)*dx()/2),-(float)(dy()/2),0);
	}
	SDL_Rect p;
	for (unsigned int i=0;i<strlen(text);i++) {
		char letter=text[i];
		if (letter==' ') continue;
		if (letter<33||letter>126) continue;

		glPushMatrix();
		p.x=i*dx();
		glTranslatef(p.x,0,0);
		drawletter(letter);
		glPopMatrix();
	}
	glPopMatrix();
}

void Font_ogl::write(C3 pos,const char *text,E_FontAnchor a) {
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(pos.x,pos.y,pos.z);
	write(text,a);
	glPopMatrix();
}
void Font_ogl::write(C3 pos,const std::string str,E_FontAnchor a) {
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(pos.x,pos.y,pos.z);
	write(str,a);
	glPopMatrix();
}
