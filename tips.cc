

#include "tips.h"


#include "random.h"
#include "font_ogl.h"

namespace el3 {



static int seed;

void Tips::roll() {
	seed=Random::sget();
}

void Tips::cycle() {
	seed++;
}


void Tips::draw() {

	std::string str;

	switch (seed%10) {
		default:
		case 0: str="Tip 1: Black spheres hurt you when picked up."; break;
		case 1: str="Tip 2: There is a way to affect the level type selection."; break;
		case 2: str="Tip 3: Start with -offline if you don't want the game to phone home."; break;
		case 3: str="Tip 4: Pressing 't' shows you the next tip."; break;
		case 4: str="Tip 5: Try starting with '-help' for an overview of commandline options."; break;
		case 5: str="Tip 6: You can set your online hiscore name and league with '-name' and '-league'."; break;
		case 6: str="Tip 7: The indicator on the weapon shows laser energy levels."; break;
		case 7: str="Tip 8: Extras may have sideeffects."; break;
		case 8: str="Tip 9: Want more/better games? Give feedback!"; break;
		case 9: str="Thanks for playing!"; break;
	}


	glColor3ub(250,250,250);
	Font_ogl::write(
		C3(320,300),
		str,
		FA_CENTER
	);
}


} //namespace
