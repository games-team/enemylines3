#include "font_ttf.h"
#include "SDL_opengl.h"
#include "SDL.h"
#include <cmath>
#include "SDL_ttf.h"



int nextpoweroftwo(int x) {
   double logbase2 = log(x) / log(2);
   return static_cast<int>((pow(2,ceil(logbase2)))+0.5);
}


void gen_dl_letter(TTF_Font *font,C3 col,char c,GLuint dl) {
	SDL_Surface *render;
	SDL_Surface *poweroftwo;
	int w,h;
	GLuint texture;

	SDL_Color color;
	color.r = col.x;
	color.g = col.y;
	color.b = col.z;

	char text[2];
	text[0]=c;
	text[1]='\0';
	
	render = TTF_RenderText_Solid(font,text,color);

	w = nextpoweroftwo(render->w);
	h = nextpoweroftwo(render->h);
	
	poweroftwo = SDL_CreateRGBSurface(0, w, h, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);

	SDL_BlitSurface(render, 0, poweroftwo, 0);
	
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_BGRA, 
			GL_UNSIGNED_BYTE, poweroftwo->pixels );
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	

	glNewList(dl,GL_COMPILE);
   glBlendFunc(GL_ONE, GL_ONE);
   glEnable(GL_BLEND);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glColor3f(1.0f, 1.0f, 1.0f);
	
	glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex2f(0    , 0);
		glTexCoord2f(1, 0); glVertex2f(0 + w, 0);
		glTexCoord2f(1, 1); glVertex2f(0 + w, 0 + h);
		glTexCoord2f(0, 1); glVertex2f(0    , 0 + h);
	glEnd();
	glDisable(GL_TEXTURE_2D);
   glDisable(GL_BLEND);
	glEndList();
	
	glFinish();
	
	SDL_FreeSurface(render);
	SDL_FreeSurface(poweroftwo);
}



unsigned int Font_ttf::gen_dl(std::string fontname,unsigned int size,C3 col) {
	if (TTF_Init()) return 0;
		
	TTF_Font *font;

   if(!(font = TTF_OpenFont(fontname.c_str(), size))) {
      std::cerr << "ttf error " << fontname << " : " <<  TTF_GetError() << std::endl;
      return 0;
   }

	unsigned int dls;

	dls=glGenLists(94);
	for (int i=33;i<127;i++) {
		int li;
		li=dls+i-33;
		gen_dl_letter(font,col,static_cast<char>(i),dls+i-33);
	}

	TTF_CloseFont(font);
	TTF_Quit();
	return dls;
}
