#include "mapswitch.h"

namespace el3 {

Mapswitch::Mapswitch(Mapbase *m) {
	m_=m;
}

unsigned int Mapswitch::dx() { return m_->dx(); }
unsigned int Mapswitch::dy() { return m_->dz(); }
unsigned int Mapswitch::dz() { return m_->dy(); }

void Mapswitch::clear(e_tiletype t) {
	m_->clear(t);
}

bool Mapswitch::inside(C3 c) {
	return m_->inside(C3(c.x,c.z,c.y));
}

void Mapswitch::set(C3 c,e_tiletype t) {
	m_->set(C3(c.x,c.z,c.y),t);
}
	
e_tiletype Mapswitch::get(C3 c) {
	return m_->get(C3(c.x,c.z,c.y));
}
void Mapswitch::lower(C3 c) {
	m_->lower(C3(c.x,c.z,c.y));
}
void Mapswitch::raise(C3 c) {
	m_->raise(C3(c.x,c.z,c.y));
}

} //namespace
