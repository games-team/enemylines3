#include "SDL.h"
#include "SDL_opengl.h"

#include "menu.h"
#include "font_ogl.h"



Menu::Menu() {
	reset();
	scale=1;
	offset=C3();
}
Menu::Menu(C3 o) {
	reset();
	offset=o;
}
void Menu::reset() {
	scale=1;
	font=1;
	font_selected=2;
	current=&baseitem;
	addcurrent=&baseitem;
	selected=0;
}

void Menu::add(Menuitem i) {
	if (addcurrent==NULL) return;
	reset();
	i.parent=addcurrent;
	addcurrent->items.push_back(i);
}

void Menu::select(int x,int y) {
	if (current==NULL) return;
	int my;
	my=y-offset.y;
	if (my<0) {
		selected=0;
		return;
	}
	if (y==0) { selected=0; return; }
	selected=my/((Font_ogl::dy()+1)*scale);
	if (selected>=static_cast<int>(current->items.size())) {
		selected=current->items.size()-1;
	}
}

void Menu::select(int s) { selected=s; }
void Menu::move_select(int c) { 
	if (current==NULL) return;
	if (c>0) {
		selected++;
		if (selected>=static_cast<int>(current->items.size())) selected=0;
		return;
	}
	if (c<0) {
		selected--;
		if (selected<0) selected=current->items.size()-1;
		return;
	}
	return;
}

void Menu::draw() {
	if (current==NULL) return;
	C3 p;
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(offset.x,offset.y,0);
	p.x=0;
	for (unsigned int i=0; i<current->items.size();i++) {
		if (static_cast<int>(i)==selected) {
			glColor3ub(250,0,0);
			Font_ogl::use_font(font_selected);
		} else {
			glColor3ub(250,250,250);
			Font_ogl::use_font(font);
		}

		p.y=i*(Font_ogl::dy()+1);
		
		glPushMatrix();
		glTranslatef(p.x,p.y,0);
		if (current->items[i].fun!=NULL) {
			current->items[i].fun(&current->items[i]);
		}
		Font_ogl::write(current->items[i].text);
		glPopMatrix();
	}
	glPopMatrix();
	Font_ogl::use_font(0);
}


Menuitem *Menu::get_item() {
	if (current==NULL) return NULL;
	if (selected<0) return NULL;
	if (selected>=static_cast<int>(current->items.size())) return NULL;
	return &current->items[selected];
}


int Menu::get() {
	if (current==NULL) return MI_NONE;
	if (selected<0) return MI_NONE;
	if (selected>=static_cast<int>(current->items.size())) return MI_NONE;
	return current->items[selected].id;
}

int Menu::click(int x,int y) {
	if (current==NULL) return MI_NONE;
	select(x,y);
	return get();
}

void Menu::pop() {
	if (current==NULL) return;
	current=current->parent;
}
void Menu::sub() {
	std::cout << " sub " << std::endl;
	if (addcurrent==NULL) return;
	if (addcurrent->items.size()==0) return;
	addcurrent=&addcurrent->items[addcurrent->items.size()-1];
}
void Menu::parent() {
	if (addcurrent==NULL) return;
	addcurrent=addcurrent->parent;
}


void Menu::handle(int id) {
	if (id==MI_POP) {
		pop();
		return;
	}
	if (id==MI_SUB) {
		current=get_item();
		return;
	}
}

int Menu::handle_event(SDL_Event event) {
	if (current==NULL) return MI_NONE;
	int r;
	switch (event.type) {
		case SDL_MOUSEMOTION: 
			//select(0,event.motion.y);
			//return MI_NONE;
			break;
		case SDL_MOUSEBUTTONDOWN:
			//r= click(event.button.x,event.button.y);
			//handle(r);
			//return r;
			break;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
				case SDLK_ESCAPE: 
					pop();
					return MI_POP;
					break;
				case SDLK_w: 
				case SDLK_UP: move_select(-1); return MI_NONE; break;
				case SDLK_s: 
				case SDLK_DOWN: move_select(1); return MI_NONE; break;
				case SDLK_RETURN:
					r=get();
					handle(r);
					return r;
					break;
				default: break;
			}
			break;
		default: break;
	}
	return MI_UNHANDLED;
}

