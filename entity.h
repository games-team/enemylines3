#ifndef __el3__entity_h
#define __el3__entity_h

#include <iostream>
#include <vector>

#include "coordinate.h"
#include "entitytype.h"
#include "tweak/tweak.h"

namespace el3 {

class Container;
class Game;

class Entity {

	C3f dest;
	Entity * mdest;
	float destroty;
	Container *container_;
	Game *game_;
	unsigned int exploding;
	unsigned int lastnewdest;

	bool snap();
	void init();
	void lookatdest();
	bool trymove(C3f d);
	bool trymove_single(C3f d);

	C3 collision;

	float speed;

	bool m_forward,m_backward,m_left,m_right;
	e_entitytype type;
public:
	bool remove;
	

	C3f rot;
	C3f pos;
	C3f dir;

	unsigned int myticks;

	unsigned int jumping;

	Entity();

	void set_type(e_entitytype t);
	void set_container(Container *c) { container_=c; }
	void set_game(Game *g) { game_=g; }

	e_entitytype get_type();

	void draw();
	bool isactive();
	

	C3f gunpos();

	void act(unsigned int ticks);
	void tick(unsigned int ticks);

	void new_dest();
	void hit();
	void explode();

	void move_direction(C3f r,float mod=1.0f);
	void move_forward(float mod=1.0f);
	void move_backward(float mod=1.0f);
	void move_left(float mod=1.0f);
	void move_right(float mod=1.0f);


	void mark_move_forward();
	void mark_move_backward();
	void mark_move_left();
	void mark_move_right();

	void turn_up();
	void turn_down();
	void turn_left();
	void turn_right();
	void turn(int x,int y);

	void jump();

};


} //namespace

#endif
