#include "SDL_opengl.h"
#include "block2_w.h"


#include <iostream>

namespace models {

static GLuint block2_w_dl=0;

void block2_w::draw() {
	dldraw();
}
void block2_w::dldraw() {
	if (block2_w_dl==0) { sdraw(); return; }
	glCallList(block2_w_dl);
}

void block2_w::gen_dl() {
	block2_w_dl=glGenLists(1);
	glNewList(block2_w_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded block2_w " << block2_w_dl << std::endl;
}
namespace mtl_block2_w {
typedef enum MTL {
m_wall,
m_last
};
} //namespace
void block2_w::material (int id) {
	switch (id) {
		case mtl_block2_w::m_wall:
static const GLfloat m_wall_diffuse[] = {0.180000,0.280720,0.280720};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_wall_diffuse);
static const GLfloat m_wall_ambient[] = {0.193333,0.334000,0.334000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_wall_ambient);
static const GLfloat m_wall_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_wall_emissive);
			break;
	}
}
float block2_w::minx() { return 0 ; }
float block2_w::miny() { return 0 ; }
float block2_w::minz() { return -2 ; }
float block2_w::maxx() { return 0.13271; }
float block2_w::maxy() { return 2; }
float block2_w::maxz() { return 1.17549e-38; }
float block2_w::radius() { return 2.0044; }
void block2_w::sdraw() {
const float vertices[8][3]={
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{0.0000000e+0,2.00000000,0.0000000e+0},
	{0.0000000e+0,0.0000000e+0,-2.00000000},
	{0.0000000e+0,2.00000000,-2.00000000},
	{0.13271040,0.22600000,-0.22600000},
	{0.13271040,1.77400000,-0.22600000},
	{0.13271040,1.77400000,-1.77400000},
	{0.13271040,0.22600000,-1.77400000},
};
const float normals[8][3]={
	{-0.92354955,0.27116069,-0.27116069},
	{-0.92354955,-0.27116069,-0.27116069},
	{-0.92354955,0.27116069,0.27116069},
	{-0.92354955,-0.27116069,0.27116069},
	{-0.96715316,0.17974254,-0.17974254},
	{-0.96715316,-0.17974254,-0.17974254},
	{-0.96715316,-0.17974254,0.17974254},
	{-0.96715316,0.17974254,0.17974254},
};
//o cube1
material(mtl_block2_w::m_wall);
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
}
} //namespace
