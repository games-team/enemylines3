#include "SDL_opengl.h"
#include "floor1.h"


#include <iostream>

namespace models {

static GLuint floor1_dl=0;

void floor1::draw() {
	dldraw();
}
void floor1::dldraw() {
	if (floor1_dl==0) { sdraw(); return; }
	glCallList(floor1_dl);
}

void floor1::gen_dl() {
	floor1_dl=glGenLists(1);
	glNewList(floor1_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded floor1 " << floor1_dl << std::endl;
}
namespace mtl_floor1 {
typedef enum MTL {
m_floor,
m_last
};
} //namespace
void floor1::material (int id) {
	switch (id) {
		case mtl_floor1::m_floor:
static const GLfloat m_floor_diffuse[] = {0.00000e+0,0.166667,1.00000};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_floor_diffuse);
static const GLfloat m_floor_ambient[] = {0.00000e+0,0.160000,1.00000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_floor_ambient);
static const GLfloat m_floor_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_floor_emissive);
			break;
	}
}
float floor1::minx() { return 0 ; }
float floor1::miny() { return 0 ; }
float floor1::minz() { return -2 ; }
float floor1::maxx() { return 2; }
float floor1::maxy() { return 0.0766395; }
float floor1::maxz() { return 1.17549e-38; }
float floor1::radius() { return 2.00147; }
void floor1::sdraw() {
const float vertices[17][3]={
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{2.00000000,0.0000000e+0,0.0000000e+0},
	{0.0000000e+0,0.0000000e+0,-2.00000000},
	{2.00000000,0.0000000e+0,-2.00000000},
	{0.49900000,7.6639466e-2,-0.49900000},
	{1.50100000,7.6639466e-2,-0.49900000},
	{1.50100000,7.6639466e-2,-1.50100000},
	{0.49900000,7.6639466e-2,-1.50100000},
	{1.00000000,7.6639466e-2,-0.33200000},
	{1.66800000,7.6639466e-2,-1.00000000},
	{1.00000000,7.6639466e-2,-1.66800000},
	{0.33200000,7.6639466e-2,-1.00000000},
	{1.00000000,7.6639466e-2,-1.00000000},
	{1.00000000,0.0000000e+0,0.0000000e+0},
	{2.00000000,0.0000000e+0,-1.00000000},
	{1.00000000,0.0000000e+0,-2.00000000},
	{0.0000000e+0,0.0000000e+0,-1.00000000},
};
const float normals[17][3]={
	{-9.5193763e-2,0.99089671,9.5193763e-2},
	{9.5193763e-2,0.99089671,9.5193763e-2},
	{-9.5193763e-2,0.99089671,-9.5193763e-2},
	{9.5193763e-2,0.99089671,-9.5193763e-2},
	{-6.3469257e-2,0.99596351,6.3469257e-2},
	{6.3469257e-2,0.99596351,6.3469257e-2},
	{6.3469257e-2,0.99596351,-6.3469257e-2},
	{-6.3469257e-2,0.99596351,-6.3469257e-2},
	{4.3531383e-18,0.99633891,8.5491434e-2},
	{8.5491434e-2,0.99633891,-8.7062766e-19},
	{-8.7062766e-19,0.99633891,-8.5491434e-2},
	{-8.5491434e-2,0.99633891,1.7412553e-18},
	{2.5917080e-18,1.00000000,0.0000000e+0},
	{3.4700702e-18,0.98537980,0.17037207},
	{0.17037207,0.98537980,-1.7350351e-18},
	{-1.7350351e-18,0.98537980,-0.17037207},
	{-0.17037207,0.98537980,3.4700702e-18},
};
//o cube1
material(mtl_floor1::m_floor);
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
}
} //namespace
