#include "SDL_opengl.h"
#include "skip.h"


#include <iostream>

namespace models {

static GLuint skip_dl=0;

void skip::draw() {
	dldraw();
}
void skip::dldraw() {
	if (skip_dl==0) { sdraw(); return; }
	glCallList(skip_dl);
}

void skip::gen_dl() {
	skip_dl=glGenLists(1);
	glNewList(skip_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded skip " << skip_dl << std::endl;
}
namespace mtl_skip {
typedef enum MTL {
m_default,
m_last
};
} //namespace
void skip::material (int id) {
	switch (id) {
		case mtl_skip::m_default:
static const GLfloat m_default_diffuse[] = {1.00000,1.00000,1.00000};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_default_diffuse);
static const GLfloat m_default_ambient[] = {1.00000,1.00000,1.00000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_default_ambient);
static const GLfloat m_default_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_default_emissive);
			break;
	}
}
float skip::minx() { return -0.221 ; }
float skip::miny() { return 0.393368 ; }
float skip::minz() { return -0.160436 ; }
float skip::maxx() { return 0.221; }
float skip::maxy() { return 0.770009; }
float skip::maxz() { return 0.160436; }
float skip::radius() { return 0.817004; }
void skip::sdraw() {
const float vertices[28][3]={
	{6.8680000e-2,0.77000945,0.16043648},
	{6.8680000e-2,0.39336833,0.16043648},
	{6.8680000e-2,0.77000945,-0.16043648},
	{6.8680000e-2,0.39336833,-0.16043648},
	{0.0000000e+0,0.39336833,0.16043648},
	{0.0000000e+0,0.77000945,0.16043648},
	{0.0000000e+0,0.39336833,-0.16043648},
	{0.0000000e+0,0.77000945,-0.16043648},
	{6.8680000e-2,0.73342993,0.12385696},
	{6.8680000e-2,0.42994785,0.12385696},
	{6.8680000e-2,0.42994785,-0.12385696},
	{6.8680000e-2,0.73342993,-0.12385696},
	{0.22100000,0.60384308,1.8083117e-2},
	{0.22100000,0.55953470,1.8083117e-2},
	{0.22100000,0.55953470,-1.8083117e-2},
	{0.22100000,0.60384308,-1.8083117e-2},
	{-6.8680000e-2,0.77000945,0.16043648},
	{-6.8680000e-2,0.39336833,0.16043648},
	{-6.8680000e-2,0.77000945,-0.16043648},
	{-6.8680000e-2,0.39336833,-0.16043648},
	{-6.8680000e-2,0.73342993,0.12385696},
	{-6.8680000e-2,0.42994785,0.12385696},
	{-6.8680000e-2,0.42994785,-0.12385696},
	{-6.8680000e-2,0.73342993,-0.12385696},
	{-0.22100000,0.60384308,1.8083117e-2},
	{-0.22100000,0.55953470,1.8083117e-2},
	{-0.22100000,0.55953470,-1.8083117e-2},
	{-0.22100000,0.60384308,-1.8083117e-2},
};
const float normals[28][3]={
	{0.81649658,0.40824829,0.40824829},
	{0.81649658,-0.40824829,0.40824829},
	{0.81649658,0.40824829,-0.40824829},
	{0.81649658,-0.40824829,-0.40824829},
	{0.0000000e+0,-0.70710678,0.70710678},
	{0.0000000e+0,0.70710678,0.70710678},
	{0.0000000e+0,-0.70710678,-0.70710678},
	{0.0000000e+0,0.70710678,-0.70710678},
	{0.94442931,0.22350805,0.24103409},
	{0.94442931,-0.22350805,0.24103409},
	{0.94442931,-0.22350805,-0.24103409},
	{0.94442931,0.22350805,-0.24103409},
	{0.89265127,0.30648410,0.33051657},
	{0.89265127,-0.30648410,0.33051657},
	{0.89265127,-0.30648410,-0.33051657},
	{0.89265127,0.30648410,-0.33051657},
	{-0.81649658,0.40824829,0.40824829},
	{-0.81649658,-0.40824829,0.40824829},
	{-0.81649658,0.40824829,-0.40824829},
	{-0.81649658,-0.40824829,-0.40824829},
	{-0.94442931,0.22350805,0.24103409},
	{-0.94442931,-0.22350805,0.24103409},
	{-0.94442931,-0.22350805,-0.24103409},
	{-0.94442931,0.22350805,-0.24103409},
	{-0.89265127,0.30648410,0.33051657},
	{-0.89265127,-0.30648410,0.33051657},
	{-0.89265127,-0.30648410,-0.33051657},
	{-0.89265127,0.30648410,-0.33051657},
};
//o cube1
material(mtl_skip::m_default);
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[22]);
	glVertex3fv(vertices[22]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[23]);
	glVertex3fv(vertices[23]);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[22]);
	glVertex3fv(vertices[22]);
	glNormal3fv(normals[23]);
	glVertex3fv(vertices[23]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
	glNormal3fv(normals[24]);
	glVertex3fv(vertices[24]);
	glNormal3fv(normals[25]);
	glVertex3fv(vertices[25]);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[25]);
	glVertex3fv(vertices[25]);
	glNormal3fv(normals[26]);
	glVertex3fv(vertices[26]);
	glNormal3fv(normals[22]);
	glVertex3fv(vertices[22]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[22]);
	glVertex3fv(vertices[22]);
	glNormal3fv(normals[26]);
	glVertex3fv(vertices[26]);
	glNormal3fv(normals[27]);
	glVertex3fv(vertices[27]);
	glNormal3fv(normals[23]);
	glVertex3fv(vertices[23]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[23]);
	glVertex3fv(vertices[23]);
	glNormal3fv(normals[27]);
	glVertex3fv(vertices[27]);
	glNormal3fv(normals[24]);
	glVertex3fv(vertices[24]);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[24]);
	glVertex3fv(vertices[24]);
	glNormal3fv(normals[27]);
	glVertex3fv(vertices[27]);
	glNormal3fv(normals[26]);
	glVertex3fv(vertices[26]);
	glNormal3fv(normals[25]);
	glVertex3fv(vertices[25]);
glEnd();
}
} //namespace
