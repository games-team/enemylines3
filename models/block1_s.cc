#include "SDL_opengl.h"
#include "block1_s.h"


#include <iostream>

namespace models {

static GLuint block1_s_dl=0;

void block1_s::draw() {
	dldraw();
}
void block1_s::dldraw() {
	if (block1_s_dl==0) { sdraw(); return; }
	glCallList(block1_s_dl);
}

void block1_s::gen_dl() {
	block1_s_dl=glGenLists(1);
	glNewList(block1_s_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded block1_s " << block1_s_dl << std::endl;
}
namespace mtl_block1_s {
typedef enum MTL {
m_wall,
m_last
};
} //namespace
void block1_s::material (int id) {
	switch (id) {
		case mtl_block1_s::m_wall:
static const GLfloat m_wall_diffuse[] = {0.280720,0.280720,0.280720};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_wall_diffuse);
static const GLfloat m_wall_ambient[] = {0.334000,0.334000,0.334000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_wall_ambient);
static const GLfloat m_wall_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_wall_emissive);
			break;
	}
}
float block1_s::minx() { return 0 ; }
float block1_s::miny() { return 0 ; }
float block1_s::minz() { return 0 ; }
float block1_s::maxx() { return 2; }
float block1_s::maxy() { return 2; }
float block1_s::maxz() { return 1.17549e-38; }
float block1_s::radius() { return 2.82843; }
void block1_s::sdraw() {
const float vertices[4][3]={
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{0.0000000e+0,2.00000000,0.0000000e+0},
	{2.00000000,2.00000000,0.0000000e+0},
	{2.00000000,0.0000000e+0,0.0000000e+0},
};
const float normals[4][3]={
	{0.0000000e+0,0.0000000e+0,1.00000000},
	{0.0000000e+0,0.0000000e+0,1.00000000},
	{0.0000000e+0,0.0000000e+0,1.00000000},
	{0.0000000e+0,0.0000000e+0,1.00000000},
};
//o cube1
material(mtl_block1_s::m_wall);
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
}
} //namespace
