#include "SDL_opengl.h"
#include "block1_e.h"


#include <iostream>

namespace models {

static GLuint block1_e_dl=0;

void block1_e::draw() {
	dldraw();
}
void block1_e::dldraw() {
	if (block1_e_dl==0) { sdraw(); return; }
	glCallList(block1_e_dl);
}

void block1_e::gen_dl() {
	block1_e_dl=glGenLists(1);
	glNewList(block1_e_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded block1_e " << block1_e_dl << std::endl;
}
namespace mtl_block1_e {
typedef enum MTL {
m_wall,
m_last
};
} //namespace
void block1_e::material (int id) {
	switch (id) {
		case mtl_block1_e::m_wall:
static const GLfloat m_wall_diffuse[] = {0.280720,0.280720,0.280720};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_wall_diffuse);
static const GLfloat m_wall_ambient[] = {0.334000,0.334000,0.334000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_wall_ambient);
static const GLfloat m_wall_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_wall_emissive);
			break;
	}
}
float block1_e::minx() { return 2 ; }
float block1_e::miny() { return 0 ; }
float block1_e::minz() { return -2 ; }
float block1_e::maxx() { return 2; }
float block1_e::maxy() { return 2; }
float block1_e::maxz() { return 1.17549e-38; }
float block1_e::radius() { return 2.82843; }
void block1_e::sdraw() {
const float vertices[4][3]={
	{2.00000000,2.00000000,0.0000000e+0},
	{2.00000000,0.0000000e+0,0.0000000e+0},
	{2.00000000,2.00000000,-2.00000000},
	{2.00000000,0.0000000e+0,-2.00000000},
};
const float normals[4][3]={
	{1.00000000,0.0000000e+0,0.0000000e+0},
	{1.00000000,0.0000000e+0,0.0000000e+0},
	{1.00000000,0.0000000e+0,0.0000000e+0},
	{1.00000000,0.0000000e+0,0.0000000e+0},
};
//o cube1
material(mtl_block1_e::m_wall);
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
}
} //namespace
