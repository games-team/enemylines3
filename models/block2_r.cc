#include "SDL_opengl.h"
#include "block2_r.h"


#include <iostream>

namespace models {

static GLuint block2_r_dl=0;

void block2_r::draw() {
	dldraw();
}
void block2_r::dldraw() {
	if (block2_r_dl==0) { sdraw(); return; }
	glCallList(block2_r_dl);
}

void block2_r::gen_dl() {
	block2_r_dl=glGenLists(1);
	glNewList(block2_r_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded block2_r " << block2_r_dl << std::endl;
}
namespace mtl_block2_r {
typedef enum MTL {
m_roof,
m_last
};
} //namespace
void block2_r::material (int id) {
	switch (id) {
		case mtl_block2_r::m_roof:
static const GLfloat m_roof_diffuse[] = {0.567100,0.567100,0.567100};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_roof_diffuse);
static const GLfloat m_roof_ambient[] = {0.500500,0.500500,0.500500};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_roof_ambient);
static const GLfloat m_roof_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_roof_emissive);
			break;
	}
}
float block2_r::minx() { return 0 ; }
float block2_r::miny() { return 2 ; }
float block2_r::minz() { return -2 ; }
float block2_r::maxx() { return 2; }
float block2_r::maxy() { return 2.04372; }
float block2_r::maxz() { return 1.17549e-38; }
float block2_r::radius() { return 2.85951; }
void block2_r::sdraw() {
const float vertices[8][3]={
	{0.0000000e+0,2.00000000,0.0000000e+0},
	{2.00000000,2.00000000,0.0000000e+0},
	{0.0000000e+0,2.00000000,-2.00000000},
	{2.00000000,2.00000000,-2.00000000},
	{0.11800000,2.04371874,-0.11800000},
	{1.88200000,2.04371874,-0.11800000},
	{1.88200000,2.04371874,-1.88200000},
	{0.11800000,2.04371874,-1.88200000},
};
const float normals[8][3]={
	{-0.17920126,0.96735403,0.17920126},
	{0.17920126,0.96735403,0.17920126},
	{-0.17920126,0.96735403,-0.17920126},
	{0.17920126,0.96735403,-0.17920126},
	{-0.11909777,0.98571367,0.11909777},
	{0.11909777,0.98571367,0.11909777},
	{0.11909777,0.98571367,-0.11909777},
	{-0.11909777,0.98571367,-0.11909777},
};
//o cube1
material(mtl_block2_r::m_roof);
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
}
} //namespace
