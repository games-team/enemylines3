#include "SDL_opengl.h"
#include "block1_r.h"


#include <iostream>

namespace models {

static GLuint block1_r_dl=0;

void block1_r::draw() {
	dldraw();
}
void block1_r::dldraw() {
	if (block1_r_dl==0) { sdraw(); return; }
	glCallList(block1_r_dl);
}

void block1_r::gen_dl() {
	block1_r_dl=glGenLists(1);
	glNewList(block1_r_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded block1_r " << block1_r_dl << std::endl;
}
namespace mtl_block1_r {
typedef enum MTL {
m_roof,
m_last
};
} //namespace
void block1_r::material (int id) {
	switch (id) {
		case mtl_block1_r::m_roof:
static const GLfloat m_roof_diffuse[] = {0.567100,0.567100,0.567100};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_roof_diffuse);
static const GLfloat m_roof_ambient[] = {0.500500,0.500500,0.500500};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_roof_ambient);
static const GLfloat m_roof_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_roof_emissive);
			break;
	}
}
float block1_r::minx() { return 0 ; }
float block1_r::miny() { return 2 ; }
float block1_r::minz() { return -2 ; }
float block1_r::maxx() { return 2; }
float block1_r::maxy() { return 2; }
float block1_r::maxz() { return 1.17549e-38; }
float block1_r::radius() { return 2.82843; }
void block1_r::sdraw() {
const float vertices[4][3]={
	{0.0000000e+0,2.00000000,0.0000000e+0},
	{2.00000000,2.00000000,0.0000000e+0},
	{0.0000000e+0,2.00000000,-2.00000000},
	{2.00000000,2.00000000,-2.00000000},
};
const float normals[4][3]={
	{0.0000000e+0,1.00000000,0.0000000e+0},
	{0.0000000e+0,1.00000000,0.0000000e+0},
	{0.0000000e+0,1.00000000,0.0000000e+0},
	{0.0000000e+0,1.00000000,0.0000000e+0},
};
//o cube1
material(mtl_block1_r::m_roof);
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
}
} //namespace
