#include "SDL_opengl.h"
#include "block1_n.h"


#include <iostream>

namespace models {

static GLuint block1_n_dl=0;

void block1_n::draw() {
	dldraw();
}
void block1_n::dldraw() {
	if (block1_n_dl==0) { sdraw(); return; }
	glCallList(block1_n_dl);
}

void block1_n::gen_dl() {
	block1_n_dl=glGenLists(1);
	glNewList(block1_n_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded block1_n " << block1_n_dl << std::endl;
}
namespace mtl_block1_n {
typedef enum MTL {
m_wall,
m_last
};
} //namespace
void block1_n::material (int id) {
	switch (id) {
		case mtl_block1_n::m_wall:
static const GLfloat m_wall_diffuse[] = {0.280720,0.280720,0.280720};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_wall_diffuse);
static const GLfloat m_wall_ambient[] = {0.334000,0.334000,0.334000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_wall_ambient);
static const GLfloat m_wall_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_wall_emissive);
			break;
	}
}
float block1_n::minx() { return 0 ; }
float block1_n::miny() { return 0 ; }
float block1_n::minz() { return -2 ; }
float block1_n::maxx() { return 2; }
float block1_n::maxy() { return 2; }
float block1_n::maxz() { return 1.17549e-38; }
float block1_n::radius() { return 2.82843; }
void block1_n::sdraw() {
const float vertices[4][3]={
	{0.0000000e+0,0.0000000e+0,-2.00000000},
	{0.0000000e+0,2.00000000,-2.00000000},
	{2.00000000,2.00000000,-2.00000000},
	{2.00000000,0.0000000e+0,-2.00000000},
};
const float normals[4][3]={
	{0.0000000e+0,0.0000000e+0,-1.00000000},
	{0.0000000e+0,0.0000000e+0,-1.00000000},
	{0.0000000e+0,0.0000000e+0,-1.00000000},
	{0.0000000e+0,0.0000000e+0,-1.00000000},
};
//o cube1
material(mtl_block1_n::m_wall);
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
}
} //namespace
