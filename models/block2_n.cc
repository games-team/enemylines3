#include "SDL_opengl.h"
#include "block2_n.h"


#include <iostream>

namespace models {

static GLuint block2_n_dl=0;

void block2_n::draw() {
	dldraw();
}
void block2_n::dldraw() {
	if (block2_n_dl==0) { sdraw(); return; }
	glCallList(block2_n_dl);
}

void block2_n::gen_dl() {
	block2_n_dl=glGenLists(1);
	glNewList(block2_n_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded block2_n " << block2_n_dl << std::endl;
}
namespace mtl_block2_n {
typedef enum MTL {
m_wall,
m_last
};
} //namespace
void block2_n::material (int id) {
	switch (id) {
		case mtl_block2_n::m_wall:
static const GLfloat m_wall_diffuse[] = {0.180000,0.280720,0.280720};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_wall_diffuse);
static const GLfloat m_wall_ambient[] = {0.193333,0.334000,0.334000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_wall_ambient);
static const GLfloat m_wall_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_wall_emissive);
			break;
	}
}
float block2_n::minx() { return 0 ; }
float block2_n::miny() { return 0 ; }
float block2_n::minz() { return -2 ; }
float block2_n::maxx() { return 2; }
float block2_n::maxy() { return 2; }
float block2_n::maxz() { return 1.17549e-38; }
float block2_n::radius() { return 2.82843; }
void block2_n::sdraw() {
const float vertices[8][3]={
	{0.0000000e+0,0.0000000e+0,-2.00000000},
	{0.0000000e+0,2.00000000,-2.00000000},
	{2.00000000,2.00000000,-2.00000000},
	{2.00000000,0.0000000e+0,-2.00000000},
	{0.22600000,0.22600000,-1.86728960},
	{0.22600000,1.77400000,-1.86728960},
	{1.77400000,1.77400000,-1.86728960},
	{1.77400000,0.22600000,-1.86728960},
};
const float normals[8][3]={
	{0.27116069,0.27116069,-0.92354955},
	{0.27116069,-0.27116069,-0.92354955},
	{-0.27116069,-0.27116069,-0.92354955},
	{-0.27116069,0.27116069,-0.92354955},
	{0.17974254,0.17974254,-0.96715316},
	{0.17974254,-0.17974254,-0.96715316},
	{-0.17974254,-0.17974254,-0.96715316},
	{-0.17974254,0.17974254,-0.96715316},
};
//o cube1
material(mtl_block2_n::m_wall);
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
}
} //namespace
