

#include <iostream>

namespace models {

static GLuint skel_dl=0;

void skel::draw() {
	dldraw();
}
void skel::dldraw() {
	if (skel_dl==0) { sdraw(); return; }
	glCallList(skel_dl);
}

void skel::gen_dl() {
	skel_dl=glGenLists(1);
	glNewList(skel_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded skel " << skel_dl << std::endl;
}
