


namespace models {

class block2_w {
	static void material(int id);
	
public:
	void draw();

	static void gen_dl();
	static void dldraw();
	static void sdraw();

	float minx();
	float miny();
	float minz();
	float maxx();
	float maxy();
	float maxz();
	float radius();



	virtual ~block2_w() {}
};


}

