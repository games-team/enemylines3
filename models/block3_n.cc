#include "SDL_opengl.h"
#include "block3_n.h"


#include <iostream>

namespace models {

static GLuint block3_n_dl=0;

void block3_n::draw() {
	dldraw();
}
void block3_n::dldraw() {
	if (block3_n_dl==0) { sdraw(); return; }
	glCallList(block3_n_dl);
}

void block3_n::gen_dl() {
	block3_n_dl=glGenLists(1);
	glNewList(block3_n_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded block3_n " << block3_n_dl << std::endl;
}
namespace mtl_block3_n {
typedef enum MTL {
m_wall,
m_last
};
} //namespace
void block3_n::material (int id) {
	switch (id) {
		case mtl_block3_n::m_wall:
static const GLfloat m_wall_diffuse[] = {0.433333,0.360300,0.326667};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_wall_diffuse);
static const GLfloat m_wall_ambient[] = {0.334000,0.334000,0.334000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_wall_ambient);
static const GLfloat m_wall_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_wall_emissive);
			break;
	}
}
float block3_n::minx() { return 0 ; }
float block3_n::miny() { return 0 ; }
float block3_n::minz() { return -2.0776 ; }
float block3_n::maxx() { return 2; }
float block3_n::maxy() { return 2; }
float block3_n::maxz() { return 1.17549e-38; }
float block3_n::radius() { return 2.82843; }
void block3_n::sdraw() {
const float vertices[12][3]={
	{0.0000000e+0,0.0000000e+0,-2.00000000},
	{0.0000000e+0,2.00000000,-2.00000000},
	{2.00000000,2.00000000,-2.00000000},
	{2.00000000,0.0000000e+0,-2.00000000},
	{0.0000000e+0,1.00000000,-2.00000000},
	{1.00000000,0.0000000e+0,-2.00000000},
	{1.00000000,2.00000000,-2.00000000},
	{2.00000000,1.00000000,-2.00000000},
	{0.23800000,1.00000000,-2.07759568},
	{1.00000000,1.76200000,-2.07759568},
	{1.76200000,1.00000000,-2.07759568},
	{1.00000000,0.23800000,-2.07759568},
};
const float normals[12][3]={
	{0.0000000e+0,0.0000000e+0,-1.00000000},
	{0.0000000e+0,0.0000000e+0,-1.00000000},
	{0.0000000e+0,0.0000000e+0,-1.00000000},
	{0.0000000e+0,0.0000000e+0,-1.00000000},
	{-0.15333151,0.0000000e+0,-0.98817481},
	{0.0000000e+0,-0.15333151,-0.98817481},
	{0.0000000e+0,0.15333151,-0.98817481},
	{0.15333151,0.0000000e+0,-0.98817481},
	{-0.20576416,0.0000000e+0,-0.97860161},
	{0.0000000e+0,0.20576416,-0.97860161},
	{0.20576416,0.0000000e+0,-0.97860161},
	{0.0000000e+0,-0.20576416,-0.97860161},
};
//o cube1
material(mtl_block3_n::m_wall);
glBegin(GL_POLYGON);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
}
} //namespace
