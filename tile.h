#ifndef __el3__tile_h
#define __el3__tile_h

#include <iostream>
#include "coordinate.h"

namespace el3 {

class Mapbase;

class Tile {

public:

	static void draw(C3 p,Mapbase *mi,int seed);
	static void draw_above(C3 p,Mapbase *mi,int seed);

};


} //namespace

#endif
