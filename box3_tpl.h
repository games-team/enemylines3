#include <iostream>
#include <sstream>
#include <utility>

using namespace std::rel_ops;

template <class T>class Box3_tpl {
public:
	C3_tpl <T> min,max;

	Box3_tpl() {clear();}
	Box3_tpl(C3_tpl <T> mi,C3_tpl <T> ma): min(mi),max(ma){}

	


	C3_tpl <T> size() { return max-min; }
	T xsize() { return max.x-min.x; }
	T ysize() { return max.y-min.y; }
	T zsize()  { return max.z-min.z; }
	C3_tpl <T> center() { return (max+min)/2; }

	void clear() {
		min.max();
		max.min();
	}


	void add(const C3_tpl <T> &p) {
		if (p.x < min.x) min.x = p.x;
		if (p.x > max.x) max.x = p.x;
		if (p.y < min.y) min.y = p.y;
		if (p.y > max.y) max.y = p.y;
		if (p.z < min.z) min.z = p.z;
		if (p.z > max.z) max.z = p.z;
	}
	

	void add(const Box3_tpl <T> &b) {
		add(b.min);
		add(b.max);
	}

	bool iswithin(C3_tpl <T> t) {
		if (t.x<min.x) return false;
		if (t.y<min.y) return false;
		if (t.z<min.z) return false;
	
		if (t.x>max.x) return false;
		if (t.y>max.y) return false;
		if (t.z>max.z) return false;

		return true;
	}


};
