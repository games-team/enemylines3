#include "SDL_opengl.h"

#include "tile.h"
#include "tiletype.h"
#include "mapbase.h"

#include "models/all.h"
#include "tweak/tweak.h"

//#include "timeoutlist.h"

#include "map.h"

namespace el3 {

void transform() {
	float tx=Tweak::f_block_transx();
	float ty=Tweak::f_block_transy();
	float tz=Tweak::f_block_transz();
	float sc=Tweak::f_block_scale();
	glTranslatef(tx,ty,tz);
	glScalef(sc,sc,sc);
}

void front(int seed) {
	glPushMatrix();
	transform();
	switch (seed%3) {
		case 0: models::block2_s::dldraw(); break;
		case 1: models::block3_s::dldraw(); break;
		case 2: models::block1_s::dldraw(); break;
	}
	glPopMatrix();
}
void back(int seed) {
	glPushMatrix();
	transform();
	switch (seed%3) {
		case 0: models::block2_n::dldraw(); break;
		case 1: models::block3_n::dldraw(); break;
		case 2: models::block1_n::dldraw(); break;
	}
	glPopMatrix();
}
void left(int seed) {
	glPushMatrix();
	transform();
	switch (seed%3) {
		case 0: models::block2_w::dldraw(); break;
		case 1: models::block3_w::dldraw(); break;
		case 2: models::block1_w::dldraw(); break;
	}
	glPopMatrix();
}
void right(int seed) {
	glPushMatrix();
	transform();
	switch (seed%3) {
		case 0: models::block2_e::dldraw(); break;
		case 1: models::block3_e::dldraw(); break;
		case 2: models::block1_e::dldraw(); break;
	}
	glPopMatrix();
}


void floorcolor(unsigned int i) {
	glColor3f(0,0,.4+i*0.01);
}
void floor(int seed) {
	glPushMatrix();
	transform();
	models::floor1::dldraw();
	glPopMatrix();
}
void roofcolor(unsigned int i) {
	float base=0.5f;
	glColor3f(base+i*0.01,base+i*0.01,base+i*0.01);
}
void roof(int seed) {
	glPushMatrix();
	glTranslatef(0,-1,0);
	transform();
	switch (seed%3) {
		case 0: models::block2_r::dldraw(); break;
		case 1: models::block3_r::dldraw(); break;
		case 2: models::block1_r::dldraw(); break;
	}
	glPopMatrix();
}


void place(Mapbase *m,C3 p,int my,int seed) {

	if (Map::typetoheight(m->get(C3(p.x,0,p.z+1)))<my) {
		front(seed);
	}
	if (Map::typetoheight(m->get(C3(p.x,0,p.z-1)))<my) {
		back(seed);
	}
	if (Map::typetoheight(m->get(C3(p.x-1,0,p.z)))<my) {
		left(seed);
	}
	if (Map::typetoheight(m->get(C3(p.x+1,0,p.z)))<my) {
		right(seed);
	}
}


void Tile::draw(C3 p,Mapbase *map,int seed) {
	e_tiletype t;
	t=map->get(p);

	if (t==TT_LEVEL0) { return; }
	glShadeModel(GL_FLAT);
	if (t==TT_LEVEL1) { floor(seed); glShadeModel(GL_SMOOTH); return; }


	glPushMatrix();
	for (int i=2;i<=Map::typetoheight(t);i++) {
		place(map,p,i,seed);
		glTranslatef(0,1,0);
	}
	roof(seed);
	glShadeModel(GL_SMOOTH);
	glPopMatrix();
}

void Tile::draw_above(C3 p,Mapbase *map,int seed) {
	e_tiletype t;
	t=map->get(p);
	if (t==TT_LEVEL0) { return; }

	glPushMatrix();
	int i;
	for (i=2;i<=Map::typetoheight(t);i++) {
		glTranslatef(0,1,0);
	}
	place(map,p,i,seed);
	roof(seed);
	glShadeModel(GL_SMOOTH);
	glPopMatrix();

}




} //namespace
