
DESTDIR	?= /usr/local

TARGET = enemylines3

CXX= g++

CXXFLAGS = -O2 -Wall `sdl-config --cflags` -I./ -Ielements/

LDFLAGS = `sdl-config --libs` -lGL -lGLU -lSDL_mixer -lSDL_ttf -lcurl


all: $(TARGET)

OFILES = audio.o config.o container.o entity.o font_data.o font.o font_ogl.o font_ttf.o frustum.o game.o help.o hiscore.o main.o mapbase.o map.o mapswitch.o menu.o random.o shot.o skybox.o sphere.o tile.o timeoutlist.o tips.o util.o generators/hole.o generators/intreg1.o generators/invert.o generators/lab.o generators/pyr.o generators/randomize_height.o generators/stamp.o generators/util.o elements/energy.o elements/goal_keys.o elements/goal_kills.o elements/goal_survive.o elements/interval.o elements/level.o elements/score.o elements/supercharge.o elements/timeleft.o models/block1_e.o models/block1_n.o models/block1_r.o models/block1_s.o models/block1_w.o models/block2_e.o models/block2_n.o models/block2_r.o models/block2_s.o models/block2_w.o models/block3_e.o models/block3_n.o models/block3_r.o models/block3_s.o models/block3_w.o models/floor1.o models/key.o models/pill.o models/robot.o models/skip.o models/weapon.o tweak/tweak_release.o

$(TARGET): $(OFILES)
	$(CXX) -o $(TARGET) $(OFILES) $(LDFLAGS)


clean:
	rm -f *.o elements/*.o models/*.o generators/*.o tweak/*.o $(TARGET)

install: 
	mkdir -p $(DESTDIR)/share/enemylines3/
	mkdir -p $(DESTDIR)/bin/
	cp $(TARGET) $(DESTDIR)/bin/
	cp data/* $(DESTDIR)/share/enemylines3/
