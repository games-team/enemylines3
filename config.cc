

#include "config.h"

namespace el3 {


bool mouse_reverse_=false;


int Config::mouse_reverse() {
	if (mouse_reverse_) return -1;
	return 1;
}

void Config::toggle_mouse_reverse() {
	mouse_reverse_=!mouse_reverse_;
}

unsigned int dx_=800;
unsigned int dy_=600;
void Config::resolution(unsigned int x,unsigned int y) {
	dx_=x;
	dy_=y;
}

unsigned int Config::dx() { return dx_; }
unsigned int Config::dy() { return dy_; }


bool record_=false;

bool Config::record(){
	return record_;
}
void Config::toggle_record() {
	record_=!record_;
}


bool show_gun_=true;

bool Config::show_gun() {
	return show_gun_;
}
void Config::toggle_show_gun() {
	show_gun_=!show_gun_;
}

int league_=0;
int Config::league() {
	return league_;
}
void Config::set_league(int l) {
	league_=l;
}

std::string name_="anon";

std::string Config::name() {
	return name_;
}
void Config::set_name(std::string n) {
	if (n=="") return;
	name_=n;
}

bool offline_=false;
bool Config::offline() {
	return offline_;
}
void Config::set_offline(bool b) {
	offline_=b;
}

} //namespace
