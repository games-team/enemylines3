
#include "SDL_opengl.h"

#include "font.h"
#include "font_ogl.h"
#include "font_data.h"
#include "font_ttf.h"

namespace el3 {


void Font::load_fallback() {
	GLuint dl;
	dl=Font_data::gen_dl();
	Font_ogl::init_font(0,dl,Font_data::dx(),Font_data::dy());
	Font_ogl::init_font(1,dl,Font_data::dx(),Font_data::dy());
	Font_ogl::init_font(2,dl,Font_data::dx(),Font_data::dy());
	Font_ogl::init_font(3,dl,Font_data::dx(),Font_data::dy());
}

bool Font::load(std::string path) {
	GLuint dl;
	#ifdef NOTTF 
		load_fallback();
		return true;
	#endif

	dl=Font_ttf::gen_dl(path+"font.ttf",11);
	if (dl==0) return false;
	Font_ogl::init_font(0,dl,6,11);

	dl=Font_ttf::gen_dl(path+"font.ttf",22);
	if (dl==0) return false;
	Font_ogl::init_font(1,dl,14,22);

	dl=Font_ttf::gen_dl(path+"font.ttf",22,C3(250,0,0));
	if (dl==0) return false;
	Font_ogl::init_font(2,dl,14,22);


	dl=Font_ttf::gen_dl(path+"font.ttf",11,C3(255,255,0));
	if (dl==0) return false;
	Font_ogl::init_font(3,dl,6,11);

	return true;
}


} //namespace
