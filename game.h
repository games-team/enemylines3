#ifndef __el3__game_h
#define __el3__game_h

#include <iostream>
#include "SDL.h"

#include "container.h"
#include "menu.h"

#include "coordinate.h"
#include "tiletype.h"

#include "energy.h"
#include "timeleft.h"
#include "interval.h"
#include "supercharge.h"
#include "score.h"
#include "level.h"
#include "goal_keys.h"
#include "goal_kills.h"
#include "goal_survive.h"
#include "frustum.h"
#include "timeoutlist.h"

namespace el3 {

class Entity;
class Map;


typedef enum E_Action {
	A_NONE,
	A_START,
	A_QUIT,
	
	A_FORWARD,
	A_BACKWARD,
	A_LEFT,
	A_RIGHT,
	A_JUMP,
	A_TURNLEFT,
	A_TURNRIGHT,
	A_TURN,
	A_FIRE,
	A_ADDENEMY,
	A_ADDEXTRA,
	A_DAMAGE,
	A_DESTROY,
	A_PICKED,
	A_ADDKEY,
	A_ADDSKIP,

	A_LAST
};


typedef enum E_GameState {
	GS_NONE,
	GS_PLAYING,
	GS_FINISHED,
	GS_NEXTLEVEL,
	GS_QUIT
};


typedef enum E_Diffi {
	D_EASY=1,
	D_MEDIUM=2,
	D_HARD=4,
	D_NIGHTMARE=10
};


typedef enum E_LevelType {
	LT_NONE,
	LT_KILLS,
	LT_TIME,
	LT_KEYS,
	LT_DEFEND,
	LT_LAST
};


class Game {
	Container container_;
	Entity * player_;
	Map *map_;
	unsigned int ticks_;
	unsigned int end_;

	Energy life_energy_;
	Energy laser_energy_;
	Energy jump_energy_;

	Supercharge supercharge;

	unsigned int lastjetpackaudio;
	Interval regen_interval;
	Interval wave_interval;
	Interval act_interval;

	Timeoutlist <C3> destroyed;

	Score score;

	Menu menu;
	bool menu_active;

	Level level;


	Goal_Keys goal_keys;
	Goal_Kills goal_kills;
	Goal_Survive goal_time;
	Goal_Survive goal_defend;

	E_Diffi difficulty;
	E_LevelType type;
	E_GameState state;
	E_LevelType skipping;

	int seed;

	Frustum frustum;

	void draw_hud();
	void draw_map();
	bool check_tile(C3 i);
	void draw_skybox();
	void draw_weapon();
	void rotrans();
	void pick(e_entitytype t);
	unsigned int num_enemies();
	void start_pos(Entity *n,bool skip=false);
	e_entitytype random_extra();
	void end_game();
public:

	Game(Game *g=NULL,E_Diffi d=D_EASY);
	~Game();


	Entity *get_player() { return player_; }
	//Score get_score() { return score; }
	unsigned int get_score() { return score.get(); }
	unsigned int get_level() { return level.get(); }
	E_Diffi get_difficulty() { return difficulty; }
	E_GameState get_state() { return state; }
	E_LevelType get_skipping() { return skipping; }

	bool isactive();

	int action(E_Action id,int x=0,int y=0);

	void cap();

	bool isvalid(C3f p,C3 *ret);
	bool invalidahead(C3f p,C3 *ret);

	void tick(unsigned int ticks);

	void draw();
	void handle_state();
	bool handle_event(SDL_Event event);
	bool handle_game_event(SDL_Event event);

	void info();
};


} //namespace

#endif
