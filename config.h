#ifndef __el3__config_h
#define __el3__config_h

#include <iostream>

namespace el3 {


class Config {

public:

	static int mouse_reverse();
	static void toggle_mouse_reverse();


	static void resolution(unsigned int x,unsigned int y);

	static unsigned int dx();
	static unsigned int dy();


	static bool record();
	static void toggle_record();

	static bool show_gun();
	static void toggle_show_gun();


	static int league();
	static void set_league(int l);

	static std::string name();
	static void set_name(std::string n);


	static bool offline();
	static void set_offline(bool b);
};


} //namespace

#endif
