#ifndef __el3_coordinate
#define __el3_coordinate
#include "c3_tpl.h"

typedef C3_tpl <int> C3;
typedef C3_tpl <int> C3i;
typedef C3_tpl <unsigned int> C3ui;
typedef C3_tpl <float> C3f;
typedef C3_tpl <double> C3d;

#include "box3_tpl.h"

#include "c4_tpl.h"
#include "matrix4_tpl.h"


typedef Box3_tpl <float> Box3f;
#endif
