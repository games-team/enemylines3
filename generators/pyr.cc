#include "../map.h"
#include "../tiletype.h"
#include "../random.h"

#include "util.h"

void pyr(el3::Mapbase *map,el3::Random *zz) {
	map->clear(el3::TT_LEVEL1);


	el3::e_tiletype types[10];

	if (zz->boolean()) {
		types[0]=el3::TT_LEVEL5;
		types[1]=el3::TT_LEVEL4;
		types[2]=el3::TT_LEVEL3;
		types[3]=el3::TT_LEVEL2;
		types[4]=el3::TT_LEVEL1;
		types[5]=el3::TT_LEVEL1;
		types[6]=el3::TT_LEVEL2;
		types[7]=el3::TT_LEVEL3;
		types[8]=el3::TT_LEVEL4;
		types[9]=el3::TT_LEVEL5;
	} else {
		types[0]=el3::TT_LEVEL1;
		types[1]=el3::TT_LEVEL2;
		types[2]=el3::TT_LEVEL3;
		types[3]=el3::TT_LEVEL4;
		types[4]=el3::TT_LEVEL5;
		types[5]=el3::TT_LEVEL5;
		types[6]=el3::TT_LEVEL4;
		types[7]=el3::TT_LEVEL3;
		types[8]=el3::TT_LEVEL2;
		types[9]=el3::TT_LEVEL1;
	}

	int s;
	s=10;
	if (zz->boolean()) s=5;

	int c;

	el3::e_tiletype t;
	c=map->dx()/2;
	for (int i=0;i<c;i++) {
		t=types[i%s];
		rect(
			map,
			i,i,
			map->dx()-i,map->dy()-i,
			t
		);
	}



	return;
}
