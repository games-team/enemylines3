#include "../map.h"
#include "../tiletype.h"
#include "../random.h"

#include "util.h"


void intreg1(el3::Mapbase *m,el3::Random *rg) {
	C3 pos,size,diff1,diff2,diff3,diff4;
	pos=C3(0,0,0);
	size=C3(0,0,0);
	diff1=C3(0,0,0);
	diff2=C3(0,0,0);
	diff3=C3(0,0,0);
	diff4=C3(0,0,0);

	el3::e_tiletype T_FLOOR=el3::TT_LEVEL1;

	int s;
	s=2+rg->get()%3;
	C3 center=C3(m->dx()/2,m->dy()/2,0);


	rect(m,center.x-s,center.y-s,center.x+s,center.y+s,T_FLOOR);


	int prog,i;



	prog=m->dy()/2-s;
	std::vector <int> h;
	h.push_back(prog/4);
	h.push_back(prog/4);
	h.push_back(prog/4);
	h.push_back(prog/4);

	h=rg->randomize(h);

	std::vector <int> b;
	b.push_back(2+rg->get()%5);
	b.push_back(2+rg->get()%8);
	b.push_back(2+rg->get()%15);
	b.push_back(2+rg->get()%5);


	prog=m->dy()/2-s;
	for (i=0;i<4;i++) {  
		rect(m,center.x-b[i]/2,prog-h[i],center.x+b[i]/2,prog,T_FLOOR);
		prog-=h[i];
	}

	prog=m->dy()/2+s;
	for (i=0;i<4;i++) {  
		rect(m,center.x-b[i]/2,prog,center.x+b[i]/2,prog+h[i],T_FLOOR);
		prog+=h[i];
	}

	if (rg->get()%3==0) {
		b.resize(0);
		b.push_back(2+rg->get()%5);
		b.push_back(2+rg->get()%8);
		b.push_back(2+rg->get()%5);
		b.push_back(2+rg->get()%15);
	}

	prog=m->dx()/2-s;
	for (i=0;i<4;i++) {  
		rect(m,prog-h[i],center.y-b[i]/2,prog,center.y+b[i]/2,T_FLOOR);
		prog-=h[i];
	}

	prog=m->dx()/2+s;
	for (i=0;i<4;i++) {  
		rect(m,prog,center.y-b[i]/2,prog+h[i],center.y+b[i]/2,T_FLOOR);
		prog+=h[i];
	}

	if (rg->get()%3==0) {
		int r=5+rg->get()%5;
		outline(m,0+r,0+r,m->dx()-1-r,m->dy()-1-r,T_FLOOR);
		r++;
		outline(m,0+r,0+r,m->dx()-1-r,m->dy()-1-r,T_FLOOR);
	}

	return;
}
