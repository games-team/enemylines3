#include "../map.h"
#include "../tiletype.h"
#include "../random.h"

#include "util.h"


void invert(el3::Mapbase *m,el3::Random *rg) {
	C3 p;
	

	el3::e_tiletype t;
	for(p.x=0;p.x<m->dx();p.x++) {
		for(p.y=0;p.y<m->dy();p.y++) {
			t=m->get(p);

			if (t==el3::TT_LEVEL5)  m->set(p,el3::TT_LEVEL1);
			if (t==el3::TT_LEVEL4)  m->set(p,el3::TT_LEVEL2);
			if (t==el3::TT_LEVEL3)  m->set(p,el3::TT_LEVEL3);
			if (t==el3::TT_LEVEL2)  m->set(p,el3::TT_LEVEL4);
			if (t==el3::TT_LEVEL1)  m->set(p,el3::TT_LEVEL5);
		}
	}
	



	return;
}
