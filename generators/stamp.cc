#include "../map.h"
#include "../tiletype.h"
#include "../random.h"

#include "util.h"



void dostamp(el3::Mapbase *map,C3 p) {
	lower(map,p);
	lower(map,p);
	lower(map,C3(p.x-1,p.y));
	lower(map,C3(p.x+1,p.y));
	lower(map,C3(p.x,p.y+1));
	lower(map,C3(p.x,p.y-1));


}

void stamp(el3::Mapbase *map,el3::Random *zz) {
	map->clear(el3::TT_LEVEL5);

	C3 p;


	int c = zz->get(870);

	for (int i=0;i<c;i++) {
		
		p.x=zz->get(map->dx());
		p.y=zz->get(map->dy());

		dostamp(map,p);
	}

	return;
}
