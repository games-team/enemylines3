#include "../map.h"
#include "../tiletype.h"
#include "../random.h"

#include "util.h"

void hole(el3::Mapbase *map,el3::Random *zz) {


	map->clear(el3::TT_LEVEL2);


	C3 p;
	for (int x=0;x<6;x++) {
		for (int y=0;y<6;y++) {
			p.x=map->dx()/2-3+x;
			p.y=map->dy()/2-3+y;
			map->set(p,el3::TT_LEVEL1);
		}
	}


	for (int i=0;i<45;i++) {

		p.x=zz->get(map->dx());
		p.y=zz->get(map->dy());
		 
		map->set(p,el3::TT_LEVEL1);
	}

	return;
}
