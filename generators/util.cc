#include "util.h"

void rect(el3::Mapbase *m,int x1,int y1,int x2,int y2,el3::e_tiletype t) {
   for (int x=x1;x<=x2;x++) {
      for (int y=y1;y<=y2;y++) {
         if (!m->inside(C3(x,y,0))) continue;
         m->set(C3(x,y,0),t);
      }
   }
}


void outline(el3::Mapbase *m,int x1,int y1,int x2,int y2,el3::e_tiletype t) {
   for (int x=x1;x<=x2;x++) {
      m->set(C3(x,y1,0),t);
      m->set(C3(x,y2,0),t);
   }
   for (int y=y1;y<=y2;y++) {
      m->set(C3(x1,y,0),t);
      m->set(C3(x2,y,0),t);
   }
}


void lower(el3::Mapbase *m,C3 p) {
	m->lower(p);
}
