#include "../map.h"
#include "../tiletype.h"
#include "../random.h"

#include "util.h"


void randomize_height(el3::Mapbase *m,el3::Random *rg) {
	C3 p;
	
	if (rg->get(13)==3) return;

	bool l3=!(rg->get(10)==5);
	bool l4=!(rg->get(10)==5);
	bool l5=!(rg->get(10)==5);

	el3::e_tiletype t;
	for(p.x=0;p.x<m->dx();p.x++) {
		for(p.y=0;p.y<m->dy();p.y++) {
			t=m->get(p);

			if (t==el3::TT_LEVEL0) { continue; }
			if (t==el3::TT_LEVEL1) { continue; }
			if (l3&&rg->get(10)==1) m->set(p,el3::TT_LEVEL3);
			if (l4&&rg->get(10)==1) m->set(p,el3::TT_LEVEL4);
			if (l5&&rg->get(10)==1) m->set(p,el3::TT_LEVEL5);
		}
	}
	



	return;
}
