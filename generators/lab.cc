#include "../map.h"
#include "../tiletype.h"
#include "../random.h"

#include "util.h"

void rec (el3::Mapbase * karte, int stx, int sty, int l, el3::e_tiletype t){

 if((stx + l) >karte->dx()){stx = karte->dx() - l-1; }
 if((sty + l) >karte->dy()){sty = karte->dy() - l-1; }
    for (int i=0;i<l;i++) {
      for (int a=0;a<l;a++) {
		karte->set(C3(stx+a,sty+i), t );
      }
  }
}

void drawline(el3::Mapbase * karte,int stx1, int sty1,int stx2, int sty2, el3::e_tiletype t){
  int a, h, llang, x, st=0;
   if(stx2 < stx1){
      llang = stx1 - stx2;
      x = -1;
      }
      else{
         llang = stx2 - stx1;
         x = 1;
         }
   if(sty2 < sty1){
     h = sty1 - sty2;
     a = -1;
     }
     else{
       h = sty2 - sty1;
       a = 1;
       }
     for(int i= 0; i <=llang; i++){
        karte->set(C3(stx1+i*x,sty1), t);
         st = stx1 +i*x;
       }
  
     for(int i= 0; i <=h; i++){
        karte->set(C3(st,sty1+i*a), t);
        }   
        
} 

void lab(el3::Mapbase *karte,el3::Random *zz) {
	el3::e_tiletype T_EINGANG=el3::TT_LEVEL1;
	el3::e_tiletype T_BODEN=el3::TT_LEVEL1;
	el3::e_tiletype T_WAND=el3::TT_LEVEL2;
	el3::e_tiletype T_TUER=el3::TT_LEVEL1;
    karte->clear(T_WAND);
    int zx = 0;
    int zy = 0;
    int lw=0, lr, lx, ly,maxi, altlr, eg;


    rec(karte,zx+18, zy+16, 3, T_EINGANG);
    for(int i = 0; i<= 6; i++){
        drawline(karte,zx+16-i*2, zy+14 -i*2, zx +22+ i*2,zy+14- i*2, T_BODEN);
        drawline(karte,zx+16-i*2, zy+20 + i*2, zx +20+ i*2,zy +20+ i*2, T_BODEN);
        drawline(karte,zx +16- i*2, zy+14 - i*2, zx+16- i*2,zy +20+  i*2, T_BODEN);
        drawline(karte,zx + 22+ i*2, zy+14 - i*2, zx+22+i*2,zy+22+  i*2, T_BODEN);
    }
    drawline(karte,zx+20, zy+19, zx+20,zy+20, T_TUER);
    drawline(karte,zx+34, zy+34, zx+18,zy+34, T_BODEN);
    drawline(karte,zx+18, zy+34, zx+18,zy+35, T_TUER);
    rec(karte,zx+17, zy+36, 3, T_EINGANG);

    
   
   zx = zx +18;
   zy = zy + 34;
    altlr = 1;
    maxi = 0;
    do{
    int sw =   zz->get(3);
    switch(sw){
    case 0: lw = 2;
                break;
   case 1: lw = 4;
                break;
    case 2: lw = 6;
                break;
    }
     lx = zx;
     ly = zy;
     lr  = zz->get(3);
        while(lr + altlr == 3 ){lr  = zz->get(3); }
       
    switch(lr){
    case 0: 
                  if(ly-lw <=23 && ly-lw >= 12 && lx <= 25 && lx >= 14) {break;}
                 if(ly ==2 ) zy = ly + lw;
                 else{
                 if(ly- lw < 2){lw = ly - 2;}                 
                  else zy = ly-lw;
                  }     
                  zx = lx;           
                  altlr = 0;
                 break;
   case 1:   
                 if(maxi <=1) {
                   zx = lx + lw;
                 }
                 else{
                  if(ly <= 23 && ly >= 12 && lx - lw <= 25 && lx -lw >= 14) {break;}
                  if(lx == 4 ) zy = lx + lw;
                 else{
                  if(lx - lw <= 4){lw = lx - 4;}
                  zx = lx - lw;
                  }
                  }
                  zy = ly;
                  altlr = 1;
                break;
    case 2:
                   if(ly <= 23 && ly >= 12 && lx + lw <= 25 && lx + lw >= 14) {break;}
                    if(lx == 34 ) zy = lx - lw;
                 else{
                  if(lx+lw >= 34){lw = 34 - lx; }               
                  zx = lx + lw;
                  }
                  zy = ly;
                   altlr = 2;
                break;
 
    }
       drawline(karte,lx,ly,zx,zy, T_TUER);
      maxi++;
     } while(maxi <= 25);
    
  
  
  lr =   zz->get(2);
  if(zx <= 18){
      eg = 1 ;
       }
    else {
      eg = -1;
      }
  switch (lr){
     case 0:
                  lx = 19 + 3 * eg;
                  ly = 14;
                  break;
     case 1:
                  lx = 19 + 5 * eg;
                  ly = 12;
                  break;
    
  } 
 
   drawline(karte,20, 20, lx,ly, T_TUER);
   drawline(karte,zx,zy, lx,ly, T_TUER);
   
 
        for(int i = 0;i<=50;i++){
        int px = zz->get(karte->dx());
        int py = zz->get(karte->dy());
        if(px<=34 && px>=4 && py >=2 && py <=32){
        el3::e_tiletype t=karte->get(C3(px,py));
         switch (t){
     case el3::TT_LEVEL1:  karte->set(C3(px,py), el3::TT_LEVEL2);
                  break;
       case el3::TT_LEVEL2:  karte->set(C3(px,py), el3::TT_LEVEL1);
                  break;
			default:
                  break;           
     }
     }  
 }  
	return;
}
