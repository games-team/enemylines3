#ifndef __el3__mapswitch_h
#define __el3__mapswitch_h


#include "coordinate.h"
#include "mapbase.h"

namespace el3 {

class Mapswitch : public Mapbase{
	Mapbase *m_;
public:

	Mapswitch(Mapbase *m);

	unsigned int dx();
	unsigned int dy();
	unsigned int dz();


	bool inside(C3 c);
	
	e_tiletype get(C3);
	void set(C3,e_tiletype);
	void clear(e_tiletype);

	void lower(C3 p);
	void raise(C3 p);
};


} //namespace

#endif
