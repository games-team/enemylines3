#include "SDL_opengl.h"

#include "font_ogl.h"
#include "coordinate.h"


#include "help.h"

namespace el3 {

const unsigned int centerx=320;
const unsigned int centery=240;

void Help::gameplay() {
   static GLuint dl=0;
   if (dl!=0) { glCallList(dl); return; }
   dl = glGenLists(1);
   glNewList(dl,GL_COMPILE_AND_EXECUTE);
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(10,350,0);

	Font_ogl::write("Shoot evil robots before they get too close.");
	glTranslatef(0,14,0);
	Font_ogl::write("There are 4 types of levels:");
	glTranslatef(0,14,0);
	Font_ogl::write("  Kill a certain number of robots.");
	glTranslatef(0,14,0);
	Font_ogl::write("  Survive until time runs out.");
	glTranslatef(0,14,0);
	Font_ogl::write("  Collect all keys.");
	glTranslatef(0,14,0);
	Font_ogl::write("  Defend the center tile.");
	glTranslatef(0,14,0);
	Font_ogl::write("The robos can't jump but they tear down walls.");
	glTranslatef(0,14,0);
	Font_ogl::write("Extras are colorcoded to match HUD.");
	//glTranslatef(0,14,0);
	//Font_ogl::write("They may have sideeffects.");
	glPopMatrix();
	glEndList();
}

void Help::mouselock() {
	glColor3f(1,1,0);
	Font_ogl::use_font(3);
	Font_ogl::write(C3(centerx,centery+60),"Click to grab mouse!",FA_CENTER);
	Font_ogl::use_font(0);
}

void Help::mouselock_off() {
	glColor3f(1,1,0);
	Font_ogl::use_font(3);
	Font_ogl::write(C3(centerx,centery+60),"Mouse grabbed.",FA_CENTER);
	Font_ogl::write(C3(centerx,centery+80),"Press 'm' if you wish to release it.",FA_CENTER);
	Font_ogl::use_font(0);
}

void Help::controls() {
   static GLuint dl=0;
   if (dl!=0) { glCallList(dl); return; }
   dl = glGenLists(1);
   glNewList(dl,GL_COMPILE_AND_EXECUTE);
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(330,350,0);

	Font_ogl::write("'m' to toggle mouselook! 'r' to invert mouse");
	glTranslatef(0,14,0);
	Font_ogl::write("w,a,s,d or arrow keys to move.");
	glTranslatef(0,14,0);
	Font_ogl::write("Left Mousebutton fires laser.");
	glTranslatef(0,14,0);
	Font_ogl::write("Space activates Jetpack.");
	glTranslatef(0,14,0);
	Font_ogl::write("Escape or p to return to menu.");
	glTranslatef(0,14,0);
	Font_ogl::write("f to toggle fullscreen, g to toggle gun.");
	glTranslatef(0,14,0);
	Font_ogl::write("v to toggle audio.");
	glTranslatef(0,14,0);
	Font_ogl::write("F1 or h for help.");
	glPopMatrix();
	glEndList();
}

void Help::fin() {
	glColor3ub(250,250,250);
	glPushMatrix();

	Font_ogl::write(C3(centerx,centery-70),"Level cleared.",FA_CENTER);
	glTranslatef(0,20,0);
	Font_ogl::write(C3(centerx,centery-50),"Click to continue.",FA_CENTER);
	glPopMatrix();
}

void Help::goal(E_LevelType t,bool level) {
	glColor3ub(250,250,250);

	glPushMatrix();
	glTranslatef(centerx,centery-70,0);

	if (level) {
		Font_ogl::write("Congratulations! New level!",FA_CENTER);
	}
	glTranslatef(0,20,0);

	switch (t) {
		case LT_KILLS: Font_ogl::write("Kill enemies to reach next level ",FA_CENTER); break;
		case LT_TIME: Font_ogl::write("   Survive to reach next level   ",FA_CENTER); break;
		case LT_KEYS: Font_ogl::write("  Find keys to reach next level  ",FA_CENTER); break;
		case LT_DEFEND: Font_ogl::write("  Defend center tile  ",FA_CENTER); break;
		default:
			Font_ogl::write("ohoh",FA_CENTER);
			break;
	}
	glPopMatrix();
}




void Help::paused() {
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(250,10,0);
	Font_ogl::use_font(1);
	Font_ogl::write("game paused");
	Font_ogl::use_font(0);
	glPopMatrix();
}

void Help::title() {
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(250,22,0);
	Font_ogl::use_font(1);
	Font_ogl::write("enemy lines 3");
	Font_ogl::use_font(0);
	glPopMatrix();
}

void Help::gameover() {
   static GLuint dl=0;
   if (dl!=0) { glCallList(dl); return; }
   dl = glGenLists(1);
   glNewList(dl,GL_COMPILE_AND_EXECUTE);
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(centerx,100,0);
	Font_ogl::write("        Game over       ",FA_CENTER);
	glTranslatef(0,14,0);
	Font_ogl::write("Press Escape to continue",FA_CENTER);
	glPopMatrix();
	glEndList();
}


} //namespace
