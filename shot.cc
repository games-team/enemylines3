#include "shot.h"

#include <cmath>
#include "SDL.h"
#include "SDL_opengl.h"


#include "config.h"
#include "entity.h"
#include "sphere.h"

#include "tweak/tweak.h"

namespace el3 {


Shot::Shot() {
	init();
}
Shot::Shot(C3f f,C3f t) {
	init();
	from=f;
	to=t;
	timestamp=SDL_GetTicks()+Tweak::i_timing_shotduration();

	if (from.y==0) from.y=Tweak::f_shot_miny();

	C3f dir;
	dir=to-from;

	dir.normalize();
	float dist=from.dist(to);
	if (dist>50) dist=50;
	unsigned int steps;
	float stepsize=0.7f;
	if (dist<2) stepsize=0.25f;
	steps=static_cast<unsigned int>(dist/stepsize);

	for (unsigned int i=0;i<steps;i++) {
		C3f p;
		p=from+dir*i*stepsize;
		positions.push_back(p);
	}
}

unsigned int Shot::lifetimepercent() {
	int max=Tweak::i_timing_shotduration();

	int left=timestamp-SDL_GetTicks();
	if (left==0) return 0;

	int one=max/100;

	return 100-(left/one);
}

void Shot::init() {
}

void Shot::draw() {
	glEnable(GL_COLOR_MATERIAL);
	glPushMatrix();
	inner_draw();
	glPopMatrix();
	glDisable(GL_COLOR_MATERIAL);
}
void Shot::inner_draw() {
	C3 col=Tweak::C_laser();;
	col.x*=lifetimepercent();
	col.y*=lifetimepercent();
	col.z*=lifetimepercent();
	glColor3ub(col.x,col.y,col.z);


	float scale=Tweak::f_scale_projectile();
	for (unsigned int i=2;i<positions.size();i++) {
		glPushMatrix();
		glTranslatef(positions[i].x,positions[i].y,positions[i].z);
		glScalef(scale,scale,scale);
		Sphere::dldraw();
		glPopMatrix();
	}




}
std::vector < Shot > shots;

void Shot::clear() {
	shots.resize(0);
}

void Shot::draw_all() {
	std::vector <Shot>::iterator it;
	
	for (it=shots.begin();it!=shots.end();it++) {
		(*it).draw();
	}
}

void Shot::tick_all(int ticks) {
	std::vector <Shot>::iterator it;
	std::vector <std::vector <Shot>::iterator> toremove;
	std::vector <std::vector <Shot>::iterator>::iterator rit;

	unsigned int time=SDL_GetTicks();

	for (it=shots.begin();it!=shots.end();it++) {

		if ((*it).timestamp>time) continue;

		toremove.push_back(it);
	}

	for (rit=toremove.begin();rit!=toremove.end();rit++) {
		shots.erase((*rit));
	}
}
void Shot::add(C3f f,C3f t) {
	Shot s(f,t);
	shots.push_back(s);
}

} //namespace
