
#include "SDL_opengl.h"

#include "timeleft.h"
#include "util.h"

namespace PRJID {
	Timeleft::Timeleft(unsigned int m) {
		max=m;
		reset();
	}
	Timeleft::~Timeleft() {
	}
	
	void Timeleft::reset() {
		current=0;
	}

	bool Timeleft::over() {
		if (current>=max) return true;
		return false;
	}


	bool Timeleft::tick(unsigned int ticks) {
		current+=ticks;
		return over();
	}

	int Timeleft::percent() {
		float one=static_cast<float>(max)/100.0f;
		float perc=static_cast <float>(current)/one;

		int r=100-static_cast<int>(perc);
		if (r<0) r=0;
		if (r>100) r=100;
		return r;
	}


	void Timeleft::dim(C3 c,C3f s,C3f t) {
		color=c;
		scale=s;
		trans=t;
	}

	void Timeleft::draw() {
		if (percent()==0) return;
		glPushMatrix();
		glTranslatef(trans.x,trans.y,trans.z);
		glScalef(scale.x,scale.y,scale.z);
		glColor3ub(color.x,color.y,color.z);
		bar(
			C3(0,0),
			C3(percent(),1)
		);
		glPopMatrix();
	}

} //namespace
