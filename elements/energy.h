#ifndef __el_energy_h
#define __el_energy_h

#include "../release.h"

#include "../coordinate.h"
#include "timeleft.h"
#include "../font_ogl.h"

#include <iostream>


namespace PRJID {

// health, laser energy etc.

class Energy {
	unsigned int max;
	unsigned int current;

	C3f scale;
	C3f trans;
	C3 color;
	Timeleft lastreduce;

	bool draw_warn;
	bool color_override;
	GLuint label_dl;
public:
	Energy(unsigned int m=100);
	~Energy();


	bool empty();
	bool can_afford(unsigned int c);

	bool recent_loss();

	void reset();

	void cap();
	void set_current(unsigned int c);
	void gain(unsigned int g);
	void reduce(unsigned int g);
	void change(int g);

	bool tick(unsigned int ticks);

	int percent();

	void override_color(C3 c);
	void warn(bool status=true);

	void label(std::string n,C3 pos);
	void dim(C3 col,C3f scal,C3f tran);
	void draw();
};


} //namespace

#endif
