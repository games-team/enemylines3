#include <sstream>


#include "goal_kills.h"

#include "../font_ogl.h"

namespace PRJID {

	Goal_Kills::Goal_Kills(unsigned int num) {
		num_kills=num;
		label_dl=0;
		reset();
	}

	Goal_Kills::~Goal_Kills() {
		if (label_dl!=0) { glDeleteLists(label_dl,1); }
	}


	void Goal_Kills::reset() {
		current=0;
	}

	void Goal_Kills::tick(unsigned int ticks) {
	}
	void Goal_Kills::cap() {
		if (current>num_kills) {current=num_kills; }
	}
	void Goal_Kills::gotone() {
		current++;
		cap();
	}

	bool Goal_Kills::finished() {
		cap();
		if (current==num_kills) return true;
		return false;
	}

	void Goal_Kills::label(std::string l,C3 p) {
			label_dl=glGenLists(1);
			glNewList(label_dl,GL_COMPILE);
			glColor3ub(250,250,250);
			Font_ogl::write(p,l);
			glEndList();
	}

	void Goal_Kills::dim(C3 p,unsigned int w,unsigned int f) {
		pos=p;
		width=w;
		font=f;
	}

	void Goal_Kills::draw() {
		std::ostringstream sstr;
		sstr.str("");
		sstr << current << " of " << num_kills;
		std::string e=sstr.str();
		sstr.str("");
		sstr.setf ( std::ios_base::right, std::ios_base::basefield );
		sstr.setf(std::ios_base::showbase);
		sstr.width ( width );
		sstr << e;
		Font_ogl::write(pos,sstr.str());

		if (label_dl!=0) {
			glCallList(label_dl);
		}

	}

} //namespace
