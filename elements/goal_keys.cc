#include <sstream>


#include "goal_keys.h"

#include "../font_ogl.h"

namespace PRJID {

	Goal_Keys::Goal_Keys(unsigned int num,unsigned int interval) {
		num_keys=num;
		label_dl=0;
		reset();
	}
	Goal_Keys::~Goal_Keys() {
		if (label_dl!=0) { glDeleteLists(label_dl,1); }
	}


	void Goal_Keys::reset() {
		current=0;
		dropped=0;
	}

	void Goal_Keys::tick(unsigned int ticks) {
	}
	void Goal_Keys::cap() {
		if (current>num_keys) {current=num_keys; }
		if (dropped>num_keys) { dropped=num_keys; }
	}
	void Goal_Keys::gotone() {
		current++;
		cap();
	}

	bool Goal_Keys::finished() {
		cap();
		if (current==num_keys) return true;
		return false;
	}

	bool Goal_Keys::drop() {
		if (dropped>=num_keys)return false;
		dropped++;
		return true;
	}

	void Goal_Keys::label(std::string l,C3 p) {
			label_dl=glGenLists(1);
			glNewList(label_dl,GL_COMPILE);
			glColor3ub(250,250,250);
			Font_ogl::write(p,l);
			glEndList();
	}

	void Goal_Keys::dim(C3 p,unsigned int w,unsigned int f) {
		pos=p;
		width=w;
		font=f;
	}

	void Goal_Keys::draw() {
		std::ostringstream sstr;
		sstr.str("");
		sstr << current << " of " << num_keys;
		std::string e=sstr.str();
		sstr.str("");
		sstr.setf ( std::ios_base::right, std::ios_base::basefield );
		sstr.setf(std::ios_base::showbase);
		sstr.width ( width );
		sstr << e;
		Font_ogl::write(pos,sstr.str());

		if (label_dl!=0) {
			glCallList(label_dl);
		}

	}

} //namespace
