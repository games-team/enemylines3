#include <sstream>


#include "level.h"

#include "../font_ogl.h"

#include "../util.h"

namespace PRJID {

	Level::Level() {
		label_dl=0;
		reset();
	}

	Level::~Level() {
		if (label_dl!=0) { glDeleteLists(label_dl,1); label_dl=0;}
	}


	void Level::reset() {
		current=1;
	}

	unsigned int Level::get() {
		return current;
	}

	void Level::set_current(unsigned int c) {
		current=c;
	}
	void Level::gain(unsigned int g) {
		current+=g;
	}
	void Level::reduce(unsigned int g) {
		if (g>=current) { 
			current=0;
		} else {
			current-=g;
		}
	}
	void Level::change(int g) {
		if (g>0) gain(g);
		if (g<0) reduce(g*-1);
	}

	void Level::label(std::string l,C3 p) {
			label_dl=glGenLists(1);
			glNewList(label_dl,GL_COMPILE);
			glColor3ub(250,250,250);
			Font_ogl::write(p,l);
			glEndList();
	}

	void Level::dim(C3 p,unsigned int w,unsigned int f) {
		pos=p;
		width=w;
		font=f;
	}

	void Level::draw() {
		std::ostringstream sstr;
		sstr.str("");
		sstr.setf ( std::ios_base::right, std::ios_base::basefield );
		sstr.setf(std::ios_base::showbase);
		sstr.width ( width );
		sstr << current;
		glColor3ub(250,250,250);
		Font_ogl::write(pos,sstr.str());

		if (label_dl!=0) {
			glCallList(label_dl);
		}
		error();

	}

} //namespace
