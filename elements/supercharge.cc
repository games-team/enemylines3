
#include "supercharge.h"

namespace PRJID {


Supercharge::Supercharge(unsigned int l) {
	supercharge=SC_NONE;
	for (unsigned int i=0;i<static_cast<unsigned int>(SC_LAST);i++) {
		length[i]=l;
	}
}

Supercharge::~Supercharge() {
}


void Supercharge::tick(unsigned int t) {
	if (supercharge==SC_NONE) return;
	left.tick(t);
}


void Supercharge::set_length(E_SuperCharge s,unsigned int l) {
	length[s]=l;
}

void Supercharge::charge(E_SuperCharge c) {
	supercharge=c;
	left=Timeleft(length[c]);
}

E_SuperCharge Supercharge::get() {
	if (supercharge==SC_NONE) return SC_NONE;
	if (left.over()) { supercharge=SC_NONE; return SC_NONE;}
	return supercharge;
}

} //namespace
