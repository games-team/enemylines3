#ifndef __el__goal_survive_h
#define __el__goal_survive_h

#include <iostream>

#include "../release.h"
#include "../coordinate.h"

#include "timeleft.h"

#include "SDL_opengl.h"

namespace PRJID {

class Goal_Survive {
	Timeleft left;


	unsigned int font;
	unsigned int width;
	C3 pos;
	GLuint label_dl;
public:
	Goal_Survive(unsigned int time=3000);
	~Goal_Survive();
	void reset();
	void cap();


	bool finished();

	void tick(unsigned int ticks);

	void label(std::string n,C3 pos);
	void dim(C3 pos,unsigned int width,unsigned int font=0);
	void draw();
};

} //namespace

#endif
