#include <sstream>


#include "goal_survive.h"

#include "../font_ogl.h"

namespace PRJID {

	Goal_Survive::Goal_Survive(unsigned int time) {
		left=Timeleft(time);
		label_dl=0;
		reset();
	}

	Goal_Survive::~Goal_Survive() {
		if (label_dl!=0) { glDeleteLists(label_dl,1); }
	}


	void Goal_Survive::reset() {
	}

	void Goal_Survive::tick(unsigned int ticks) {
		left.tick(ticks);
	}

	bool Goal_Survive::finished() {
		//if (current==num_kills) return true;
		if (left.over()) return true;
		return false;
	}

	void Goal_Survive::label(std::string l,C3 p) {
			label_dl=glGenLists(1);
			glNewList(label_dl,GL_COMPILE);
			glColor3ub(250,250,250);
			Font_ogl::write(p,l);
			glEndList();
	}

	void Goal_Survive::dim(C3 p,unsigned int w,unsigned int f) {
		pos=p;
		width=w;
		font=f;
	}

	void Goal_Survive::draw() {
		std::ostringstream sstr;
		sstr.str("");
		sstr.setf ( std::ios_base::right, std::ios_base::basefield );
		sstr.setf(std::ios_base::showbase);
		sstr.width ( width );
		sstr << (left.get_left()/1000);
		Font_ogl::write(pos,sstr.str());

		if (label_dl!=0) {
			glCallList(label_dl);
		}

	}

} //namespace
