#ifndef __el__score_h
#define __el__score_h

#include <iostream>

#include "../release.h"
#include "../coordinate.h"

#include "SDL_opengl.h"

namespace PRJID {

class Score {
	unsigned int current;
	unsigned int font;
	unsigned int width;
	C3 pos;
	GLuint label_dl;
public:
	Score();
	~Score();
	void reset();
	
	void set_current(unsigned int c);
	unsigned int get() { return current; }

	void gain(unsigned int g);
	void reduce(unsigned int g);
	void change(int g);

	void label(std::string n,C3 pos);
	void dim(C3 pos,unsigned int width,unsigned int font=0);
	void draw();
};

std::ostream& operator<<(std::ostream&s, Score);

} //namespace

#endif
