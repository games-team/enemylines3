#ifndef __el__goal_keys_h
#define __el__goal_keys_h

#include <iostream>

#include "../release.h"
#include "../coordinate.h"

#include "SDL_opengl.h"

namespace PRJID {

class Goal_Keys {
	unsigned int current;

	unsigned int num_keys;
	unsigned int dropped;

	unsigned int font;
	unsigned int width;
	C3 pos;
	GLuint label_dl;
public:
	Goal_Keys(unsigned int num=3,unsigned int interval=2000);
	~Goal_Keys();
	void reset();
	void cap();

	void gotone();

	bool finished();

	bool drop();

	void tick(unsigned int ticks);

	void label(std::string n,C3 pos);
	void dim(C3 pos,unsigned int width,unsigned int font=0);
	void draw();
};

} //namespace

#endif
