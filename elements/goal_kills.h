#ifndef __el__goal_kills_h
#define __el__goal_kills_h

#include <iostream>

#include "../release.h"
#include "../coordinate.h"

#include "SDL_opengl.h"

namespace PRJID {

class Goal_Kills {
	unsigned int current;
	unsigned int num_kills;


	unsigned int font;
	unsigned int width;
	C3 pos;
	GLuint label_dl;
public:
	Goal_Kills(unsigned int num=30);
	~Goal_Kills();
	void reset();
	void cap();

	void gotone();

	bool finished();

	void tick(unsigned int ticks);

	void label(std::string n,C3 pos);
	void dim(C3 pos,unsigned int width,unsigned int font=0);
	void draw();
};

} //namespace

#endif
