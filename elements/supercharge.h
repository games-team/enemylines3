#ifndef __el__supercharge_h
#define __el__supercharge_h

#include <iostream>

#include "../release.h"

#include "timeleft.h"

namespace PRJID {

typedef enum E_SuperCharge {
   SC_NONE,

   SC_LIFE,
   SC_JUMP,
   SC_LASER,

   SC_WIRE,
   SC_FREEZE,
   SC_NOLASER,

	SC_LAST,
};


// supercharge something ... only one can be active at a time
// time runs out
class Supercharge {
	E_SuperCharge supercharge;
	Timeleft left;
	unsigned int length[SC_LAST];
public:
	Supercharge(unsigned int len=100);
	~Supercharge();
	void set_length(E_SuperCharge s,unsigned int l);
	
	void charge(E_SuperCharge c);
	E_SuperCharge get();

	void tick(unsigned int ticks);
};


} //namespace

#endif
