#ifndef __el__interval_h
#define __el__interval_h

#include <iostream>

#include "../release.h"

namespace PRJID {


// wave or regen intervals
class Interval {
	unsigned int last;
	unsigned int ticks;
	unsigned int length;
public:
	Interval(unsigned int len=100);
	~Interval();

	void tick(unsigned int ticks);
	bool check();
};

std::ostream& operator<<(std::ostream&s, Interval);

} //namespace

#endif
