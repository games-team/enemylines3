#ifndef __el3__help_h
#define __el3__help_h

#include <iostream>

#include "game.h"

namespace el3 {


class Help {

public:

static void controls();
static void gameplay();
static void goal(E_LevelType,bool level=false);

static void paused();
static void title();

static void fin();

static void gameover();
static void mouselock();
static void mouselock_off();

};


} //namespace

#endif
