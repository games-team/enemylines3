#ifndef __el3__hiscore_h
#define __el3__hiscore_h

#include <iostream>

namespace el3 {

class Hiscore {
public:
	static void report(unsigned int score,unsigned int difficulty,unsigned int level);

	static char *get();

	static void draw();
};

std::ostream& operator<<(std::ostream&s, Hiscore);

} //namespace

#endif
