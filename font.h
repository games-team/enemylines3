#ifndef __el3__font_h
#define __el3__font_h

#include <iostream>

namespace el3 {


class Font {

public:
	static bool load(std::string path);
	static void load_fallback();
};

} //namespace

#endif
