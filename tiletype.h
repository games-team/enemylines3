#ifndef __el3__tiletype_h
#define __el3__tiletype_h

#include <iostream>

namespace el3 {

typedef enum e_tiletype {
	TT_NONE,
	TT_LEVEL0,
	TT_LEVEL1,
	TT_LEVEL2,
	TT_LEVEL3,
	TT_LEVEL4,
	TT_LEVEL5,
	TT_DEFEND,
	TT_LAST
};


} //namespace

#endif
