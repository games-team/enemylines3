#ifndef __font_data_h
#define __font_data_h

#include <iostream>


class Font_data{
	static char get_field(int letter,int x,int y);
	static void drawletter_nodl(char letter,bool smooth);
public:
	static unsigned int gen_dl();
	static int dx();
	static int dy();
};



#endif
