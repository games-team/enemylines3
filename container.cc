#include "SDL.h"
#include "SDL_opengl.h"


#include "container.h"
#include "entity.h"
#include "random.h"

namespace PRJID {


Container::Container() {
}
Container::~Container() {
	clear();
}

void Container::clear() {
}

void Container::add(Entity *e,bool special) {
	e->set_container(this);
	entities.push_back(e);
}

void Container::tick(unsigned int ticks) {
	if (ticks==0) return;
	std::list <Entity *>::iterator it;
	std::list <std::list <Entity *>::iterator>::iterator dit;
	std::list <std::list <Entity *>::iterator> todelete;

	for (it=entities.begin();it!=entities.end();it++) {
		(*it)->tick(ticks);
		if ((*it)->remove) {
			delete (*it);
			todelete.push_back(it);
		}
	}

	for (dit=todelete.begin();dit!=todelete.end();dit++) {
		entities.erase((*dit));
	}

}

void Container::act(unsigned int ticks) {
	std::list <Entity *>::iterator it;

	for (it=entities.begin();it!=entities.end();it++) {
		if ((*it)->get_type()==ET_PLAYER) continue;
		(*it)->act(ticks);
	}
}

void Container::draw() {
	std::list <Entity *>::iterator it;

	for (it=entities.begin();it!=entities.end();it++) {
		if ((*it)==NULL) continue;
		if ((*it)->get_type()==ET_PLAYER) continue;
		(*it)->draw();
	}
}

Entity *Container::select(int x,int y) {
   glReadBuffer(GL_BACK);

	Entity *selected=NULL;
	selected=select_single(x,y);
	//SDL_GL_SwapBuffers(); SDL_Delay(500);
	if (selected!=NULL) return selected;
	return NULL;
}

Entity *Container::select_single(int x,int y) {
	std::list <Entity *>::iterator it;

	Entity *selected=NULL;
	unsigned int p;
	unsigned int rem=std::numeric_limits<unsigned int>::max();

	glReadPixels(x,y,1,1,GL_DEPTH_COMPONENT, GL_UNSIGNED_INT,static_cast<GLvoid *>(&p));
	rem=p;

	for (it=entities.begin();it!=entities.end();it++) {
		if ((*it)==NULL) continue;
		if ((*it)->get_type()==ET_PLAYER) continue;
		if (!(*it)->isactive()) continue;
		(*it)->draw();
		glReadPixels(x,y,1,1,GL_DEPTH_COMPONENT, GL_UNSIGNED_INT,static_cast<GLvoid *>(&p));
		if (p>=rem) continue;
		rem=p;
		selected=(*it);
	}
	return selected;
}

void Container::set_game(Game *g) {
	game=g;
}


Entity * Container::create(e_entitytype t) {
	Entity *n;
	n=new Entity();
	n->set_game(game);
	n->set_container(this);
	n->set_type(t);
	add(n,true);
	return n;
}


} //namespace
