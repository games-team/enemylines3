#ifndef __el__audio_h
#define __el__audio_h

#include <iostream>

namespace el3 {


typedef enum E_AudioSample {
	AS_NONE,
	AS_TITLE,
	AS_FAILED,
	AS_PASSED,
	AS_EXPLOSION,
	AS_PROBLEM,
	AS_LASER,
	AS_JETPACK,
	AS_PICK,
	AS_HURT,
	AS_LAST
};

typedef enum E_AudioChannel {
	AC_NEXT=-1,
	AC_TITLE,
	AC_LASER,
	AC_PROBLEM,
	AC_JETPACK,
	AC_PICK,
	AC_HURT,
	AC_DAMAGED,
	AC_EXPLOSION,
	AC_EXPLOSION2,
	AC_EXPLOSION3,
	AC_EXPLOSION4,
	AC_LAST
};

class Audio{
public:
	static bool init();
	static bool load(std::string dir);

	static void off();
	static void on();
	static void toggle();
	static void tick();

	static void play(E_AudioSample s,E_AudioChannel chan=AC_NEXT,int repeat=0);

	static void schedule_play(unsigned int delay,E_AudioSample s,E_AudioChannel chan=AC_NEXT,int repeat=0);
};


} //namespace

#endif
