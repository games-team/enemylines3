#ifndef __el3__map_h
#define __el3__map_h

#include <iostream>

#include "coordinate.h"
#include "mapbase.h"


#define MX 40
#define MZ 40

namespace el3 {

class Map : public Mapbase{
	void init();
	e_tiletype data[MX][MZ];
public:

	Map();

	unsigned int dx();
	unsigned int dy();
	unsigned int dz();


	bool inside(C3 c);
	
	e_tiletype get(C3);
	void set(C3,e_tiletype);

	void clear(e_tiletype t);
	
	void destroy(C3 p);

	static int typetoheight(e_tiletype t);

	bool isvalid(C3 p,C3 *ret);
	bool isvalid(C3 p,float y,C3 *ret);
	bool isvalid(e_tiletype t,float y);

	bool isvalid(C3f p,C3 *ret);
	bool invalidahead(C3f p,C3 *ret);

	void lower(C3 p);
	void raise(C3 p);
};

std::ostream& operator<<(std::ostream&s, Map);

} //namespace

#endif
