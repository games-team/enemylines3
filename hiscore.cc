#include <sstream>
#ifndef NOCURL
#include <curl/curl.h>
//#include <curl/types.h>
#include <curl/easy.h>
#endif
#include "SDL.h"
#include "SDL_thread.h"

#include "hiscore.h"
#include "coordinate.h"
#include "font_ogl.h"
#include "config.h"

#include "tweak/tweak.h"


namespace el3 {

#ifndef NOCURL
class MemoryStruct {
public:
	MemoryStruct() { reset(); }
	void reset() { if (memory!=NULL) free(memory); memory=NULL; size=0; }
  char *memory;
  size_t size;
};
MemoryStruct chunk;
bool ready=false;
bool cleaned=false;
SDL_Thread *thread;

SDL_mutex *mutex=NULL;



size_t WriteMemoryCallback(void *ptr, size_t size, size_t nmemb, void *data) {
  register int realsize = size * nmemb;
  MemoryStruct *mem = (MemoryStruct *)data;
  
  mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
  if (mem->memory) {
    memcpy(&(mem->memory[mem->size]), ptr, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
  }
  return realsize;
}


int threadfun(void *data) {
	CURL *curl_handle;

	chunk.reset();

	curl_global_init(CURL_GLOBAL_ALL);
	curl_handle = curl_easy_init();
	curl_easy_setopt(curl_handle, CURLOPT_URL, (char *)data);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	curl_easy_perform(curl_handle);
	curl_easy_cleanup(curl_handle);
	ready=true;
	return 1;
}


#endif


void Hiscore::report(unsigned int score,unsigned int difficulty,unsigned int level) {
	if (Config::offline()) return;
#ifndef NOCURL

	std::ostringstream sstr;

	sstr << "http://score.phk.at/report.php?g=1";
	sstr << "&s=" << score;
	sstr << "&l=" << level;
	sstr << "&d=" << difficulty;
	sstr << "&n=" << Config::name();
	sstr << "&t=" << Config::league();

	std::cout << "url: " << sstr.str() << std::endl;

	ready=false;

	if (mutex==NULL) {
		mutex=SDL_CreateMutex();
	}

	cleaned=false;
	thread=SDL_CreateThread(threadfun, (void *)(sstr.str().c_str()));
	if ( thread == NULL ) {
		std::cout << "no threading " << std::endl;
		return;
	}
#endif
}

#ifndef NOCURL
std::string next_part(int *p,std::string str,std::string search) {
   std::string s;
   s=str.substr(*p,str.length());
   int np=s.find(search);
   if (np==std::string::npos) { (*p)=-1; return s; }
   s=str.substr(*p,np);
   (*p)+=np+1;
   return s;
}
#endif


void Hiscore::draw() {
	#ifndef NOCURL
	if (Config::offline()) return;
	if (!ready) return;
	if (!cleaned) {
		cleaned=true;
		SDL_WaitThread(thread, NULL);
		thread=NULL;
	}
	std::cout << "chunk " << chunk.memory << std::endl;

	std::string str;
	std::string line;

	if (chunk.memory==NULL) {
		//str="";
		return;
	} else if (chunk.memory=="") {
		return;
	} else {
		str=chunk.memory;
	}


	
	int start_y=Tweak::i_hiscore_start_y();
	int move_x=Tweak::i_hiscore_move_x();

	C3 p=C3(20,start_y);

 	int a=0;
   while (a!=-1) {
		line=next_part(&a,str,"\n");
		if (line=="") continue;
		if (line=="<br>") { 
			p.x+=move_x; 
			p.y=start_y; 
			continue; 
		}
		Font_ogl::write(p,line);
		p.y+=14;
   }

	#endif
}

} //namespace
