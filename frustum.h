#include "SDL_opengl.h"
#include <string>

class Frustum {
	bool changed_;
	C4_tpl <float> planes[6];
public:

	Frustum();
	void calculate();
	void calculate_if();
	bool test(Box3_tpl <float> b);
	bool test_point(C3_tpl <float> p);
	std::string tostring();

	
	void changed();

};
