
#include "SDL_opengl.h"

#include "skybox.h"
#include "coordinate.h"
#include "tweak/tweak.h"

namespace el3 {

GLuint dl=0;

void Skybox::draw() {
	if (dl==0) {
		inner_draw();
		return;
	}
	glCallList(dl);
}

void Skybox::gen_dl(int seed) {
	if (dl!=0) {
		glDeleteLists(dl,1);
		dl=0;
	}
   dl=glGenLists(1);
   glNewList(dl,GL_COMPILE);
   inner_draw(seed);
   glEndList();
   //std::cout << " loaded skybox " << dl << std::endl;
}

void Skybox::inner_draw(int seed) {
   glPushMatrix();
   float size=41;
   glTranslatef(-size,-size,-size);
   glScalef(size*2,size*2,size*2);

	C3 colors[8]= { C3(), C3(), C3(), C3(), C3(), C3(), C3(), C3()};


static const float normals[6][3] = {
	{1,0,0},
	{-1,0,0},
	{0,1,0},
	{0,-1,0},
	{0,0,1},
	{0,0,-1}
};

static const float vertices[8][3] = {
   {1.0f,1.0f,1.0f},
   {1.0f,1.0f,0.0f},
   {0.0f,1.0f,0.0f},
   {0.0f,1.0f,1.0f},
   {1.0f,0.0f,1.0f},
   {1.0f,0.0f,0.0f},
   {0.0f,0.0f,1.0f},
   {0.0f,0.0f,0.0f}
};
static const unsigned int faceindices[6][4] = {
   {4,3,8,7},
   {2,1,5,6},
   {5,7,8,6},
   {2,3,4,1},
   {6,8,3,2},
   {1,4,7,5}
};

	switch (seed%3) {
		case 0:
			colors[0]=Tweak::C_skybox1_1();
			colors[1]=Tweak::C_skybox1_2();
			colors[2]=Tweak::C_skybox1_3();
			colors[3]=Tweak::C_skybox1_4();
			colors[4]=Tweak::C_skybox1_5();
			colors[5]=Tweak::C_skybox1_6();
			colors[6]=Tweak::C_skybox1_7();
			colors[7]=Tweak::C_skybox1_8();
			break;
		case 1:
			colors[0]=Tweak::C_skybox2_1();
			colors[1]=Tweak::C_skybox2_2();
			colors[2]=Tweak::C_skybox2_3();
			colors[3]=Tweak::C_skybox2_4();
			colors[4]=Tweak::C_skybox2_5();
			colors[5]=Tweak::C_skybox2_6();
			colors[6]=Tweak::C_skybox2_7();
			colors[7]=Tweak::C_skybox2_8();
			break;
		case 2:
			colors[0]=Tweak::C_skybox3_1();
			colors[1]=Tweak::C_skybox3_2();
			colors[2]=Tweak::C_skybox3_3();
			colors[3]=Tweak::C_skybox3_4();
			colors[4]=Tweak::C_skybox3_5();
			colors[5]=Tweak::C_skybox3_6();
			colors[6]=Tweak::C_skybox3_7();
			colors[7]=Tweak::C_skybox3_8();
			break;
	}


	glDisable(GL_LIGHTING);

	glColor3ub(100,30,200);
   glBegin(GL_QUADS);

	for (unsigned int i=0;i<6;i++) {

		glNormal3fv(normals[i]);
		C3 col;
		for (unsigned int v=0;v<4;v++) {
			col=colors[faceindices[i][v]-1];
			glColor3ub(col.x,col.y,col.z);
			glVertex3fv(vertices[faceindices[i][v]-1]);
		}



	}
   glEnd();

	glEnable(GL_LIGHTING);
   glPopMatrix();
}


} //namespace
