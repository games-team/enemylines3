#ifndef __el__container_h
#define __el__container_h

#include <iostream>
#include <list>

#include "entitytype.h"
#include "release.h"
#include "coordinate.h"

namespace PRJID {

class Entity;
class Game;


class Container {
	std::list <Entity *> entities;
	Game *game;
public:
	Container();
	~Container();

	void clear();
	void remove(unsigned int i);

	void add(Entity *e,bool special=false);

	void set_game(Game *g);
	
	void act(unsigned int ticks);
	void tick(unsigned int ticks);

	void draw();

	Entity *select(int x,int y);
	Entity *select_single(int x,int y);

	Entity *create(e_entitytype t);
};


} //namespace

#endif
