#ifndef __font_ogl
#define __font_ogl

#include <string>
#include "coordinate.h"

typedef enum E_FontAnchor {
	FA_CENTER,
	FA_NW,
};


class Font_ogl {
public:
	static void init_font(unsigned int f,unsigned int dl,unsigned int dx,unsigned int dy);
	static unsigned int dx();
	static unsigned int dy();


	static void use_font(unsigned int f);

	static void drawletter(char letter);


	static void write(const char *text,E_FontAnchor a=FA_NW);

	static void write(const std::string str,E_FontAnchor a=FA_NW);

	static void write(C3 pos,const char *text,E_FontAnchor a=FA_NW);
	static void write(C3 pos,const std::string str,E_FontAnchor a=FA_NW);
};

#endif
